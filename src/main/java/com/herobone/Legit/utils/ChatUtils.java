package com.herobone.Legit.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.herobone.Legit.Wrapper;
import net.minecraft.util.EnumTypeAdapterFactory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@SideOnly(Side.CLIENT)
public class ChatUtils {

    private static final Gson GSON;

    public static void displayMessage(final String s) {
        Wrapper.getMinecraft().player.sendMessage((ITextComponent)GSON.fromJson("{text:\"" + s + "\"}", ITextComponent.class));
    }

    static {
        GsonBuilder gsonbuilder = new GsonBuilder();
        gsonbuilder.registerTypeHierarchyAdapter(ITextComponent.class, new ITextComponent.Serializer());
        gsonbuilder.registerTypeHierarchyAdapter(Style.class, new Style.Serializer());
        gsonbuilder.registerTypeAdapterFactory(new EnumTypeAdapterFactory());
        GSON = gsonbuilder.create();
    }
}