package com.herobone.Legit.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.math.MathHelper;

public class EntityStuff
{
  
  public float[] faceEntity(Entity entityIn, boolean instant)
  {
    Minecraft mc = Minecraft.getMinecraft();
    
    double var4 = entityIn.posX - mc.player.posX;
    double var8 = entityIn.posZ - mc.player.posZ;
    
    EntityLivingBase var14 = (EntityLivingBase)entityIn;
    double var6 = var14.posY + var14.getEyeHeight() * 0.6D - (
      mc.player.posY + mc.player.getEyeHeight());
    
    double var141 = MathHelper.sqrt(var4 * var4 + var8 * var8);
    float var12 = (float)(Math.atan2(var8, var4) * 180.0D / 3.141592653589793D) - 90.0F;
    float var13 = (float)-(Math.atan2(var6, var141) * 180.0D / 3.141592653589793D);
    
    float agr = 0.5f;
    float add = agr > 0.0F ? agr : 0.2F;
    
    add *= add;
    
    float[] rot = { updateRotation(mc.player.rotationYaw, var12, add),
      updateRotation(mc.player.rotationPitch, var13, 0.7F) };
    
    return rot;
  }
  
  private float updateRotation(float jyaw, float aquaPitch, float mcFl)
  {
    float var4 = MathHelper.wrapDegrees(aquaPitch - jyaw);
    float tog = var4 + mcFl;
    if (tog < 0.0F) {
      mcFl -= tog / 6.0F;
    } else if (tog > 0.0F) {
      mcFl += tog / 6.0F;
    }
    if (var4 > mcFl) {
      var4 = mcFl;
    }
    if (var4 < -mcFl) {
      var4 = -mcFl;
    }
    return jyaw + var4;
  }
}
