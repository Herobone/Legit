package com.herobone.Legit.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;

public class UsefullStuff {

    public static int findItemSouldBe(String id)
    {
        for (int i = 9; i < 45; i++)
        {
            ItemStack stack = Minecraft.getMinecraft().player.inventoryContainer.getSlot(i).getStack();
            if (stack != null)
            {
                String x = stack.getItem().getUnlocalizedName().toLowerCase();
                if (x.contains(id)) {
                    return i;
                }
                if (((stack.getItem() instanceof ItemFood)) && (id.contains("food"))) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static float getArrowVelocity(int charge)
    {
        float f = (float)charge / 20.0F;
        f = (f * f + f * 2.0F) / 3.0F;

        if (f > 1.0F)
        {
            f = 1.0F;
        }

        return f;
    }

}
