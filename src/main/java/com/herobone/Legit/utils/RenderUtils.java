package com.herobone.Legit.utils;

import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.module.modules.render.ESP;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import org.lwjgl.opengl.GL11;

import java.awt.*;

public class RenderUtils {

	public static void drawRect(float left, float top, float rigth, float bottom, int color) {
		float f = (float)(color >> 24 & 255) / 255.0F;
		float f1 = (float)(color >> 16 & 255) / 255.0F;
		float f2 = (float)(color >> 8 & 255) / 255.0F;
		float f3 = (float)(color & 255) / 255.0F;
		GL11.glEnable(3042);
		GL11.glDisable(3553);
		GL11.glBlendFunc(770, 771);
		GL11.glEnable(2848);
		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glBegin(7);
		GL11.glVertex2d((double)rigth, (double)top);
		GL11.glVertex2d((double)left, (double)top);
		GL11.glVertex2d((double)left, (double)bottom);
		GL11.glVertex2d((double)rigth, (double)bottom);
		GL11.glEnd();
		GL11.glPopMatrix();
		GL11.glEnable(3553);
		GL11.glDisable(3042);
		GL11.glDisable(2848);
	}

	public static Color rainbowEffect(int delay) {
		float hue = (float) (System.nanoTime() + delay) / 2.0E10F % 1.0F;
		Color color = new Color((int) Long.parseLong(Integer.toHexString(Color.HSBtoRGB(hue, 1.0F, 1.0F)), 16));
		return new Color(color.getRed() / 255.0F, color.getGreen() / 255.0F, color.getBlue() / 255.0F, color.getAlpha() / 255.0F);
	}

	public static int rainbow(int delay) {
		double rainbowState = Math.ceil((System.currentTimeMillis() + delay) / 20.0);
		rainbowState %= 360;
		return Color.getHSBColor((float) (rainbowState / 360.0f), 0.8f, 0.7f).getRGB();
	}

	public static void blockESPBox(BlockPos blockPos)
	{
		double x =
				blockPos.getX();
		double y =
				blockPos.getY();
		double z =
				blockPos.getZ();
        GlStateManager.depthMask(false);
        GlStateManager.disableTexture2D();
        GlStateManager.disableLighting();
        GlStateManager.disableCull();
		if (ModuleManager.getModule(ESP.class) != null)
            GL11.glLineWidth(((ESP) ModuleManager.getModule(ESP.class)).lineWidth.getObject());
		GL11.glEnable(2848);
        GlStateManager.disableDepth();
        AxisAlignedBB var12 = new AxisAlignedBB(x, y, z,x + 1, y + 1, z + 1);
        RenderGlobal.drawSelectionBoundingBox(var12, 0, 0, 0, 255);
        GlStateManager.enableDepth();
        GlStateManager.enableTexture2D();
        GlStateManager.enableLighting();
        GlStateManager.enableCull();
        GlStateManager.depthMask(true);
	}

	public static void drawOutlinedBoundingBox(AxisAlignedBB p_drawOutlinedBoundingBox_0_, int p_drawOutlinedBoundingBox_1_, int p_drawOutlinedBoundingBox_2_, int p_drawOutlinedBoundingBox_3_, int p_drawOutlinedBoundingBox_4_) {
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		bufferbuilder.begin(3, DefaultVertexFormats.POSITION_COLOR);
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.maxX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.maxX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.maxZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.maxZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		tessellator.draw();
		bufferbuilder.begin(3, DefaultVertexFormats.POSITION_COLOR);
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.maxX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.maxX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.maxZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.maxZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		tessellator.draw();
		bufferbuilder.begin(1, DefaultVertexFormats.POSITION_COLOR);
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.maxX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.maxX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.minZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.maxX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.maxZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.maxX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.maxZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.minY, p_drawOutlinedBoundingBox_0_.maxZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		bufferbuilder.pos(p_drawOutlinedBoundingBox_0_.minX, p_drawOutlinedBoundingBox_0_.maxY, p_drawOutlinedBoundingBox_0_.maxZ).color(p_drawOutlinedBoundingBox_1_, p_drawOutlinedBoundingBox_2_, p_drawOutlinedBoundingBox_3_, p_drawOutlinedBoundingBox_4_).endVertex();
		tessellator.draw();
	}

	public static void boxESP(BlockPos blockPos, int red, int green, int blue, int alpha) {
        double x =
                blockPos.getX() - TileEntityRendererDispatcher.staticPlayerX;
        double y =
                blockPos.getY()- TileEntityRendererDispatcher.staticPlayerY;
        double z =
                blockPos.getZ()- TileEntityRendererDispatcher.staticPlayerZ;

        //LegitBase.CLIENT_INSTANCE.LOGGER.info("Try to render SpecialESP");

        GlStateManager.depthMask(false);
        GlStateManager.disableTexture2D();
        GlStateManager.disableLighting();
        GlStateManager.disableCull();
        GL11.glLineWidth(2f);
        GL11.glEnable(2848);
        GlStateManager.disableDepth();
        AxisAlignedBB var12 = new AxisAlignedBB(x, y, z,x + 1, y + 1, z + 1);
        RenderUtils.drawOutlinedBoundingBox(var12, red, green, blue, alpha);
        GlStateManager.enableDepth();
        GlStateManager.enableTexture2D();
        GlStateManager.enableLighting();
        GlStateManager.enableCull();
        GlStateManager.depthMask(true);
    }

	public static void specESP(TileEntity bed, int red, int green, int blue, int alpha) {

        AxisAlignedBB ABB = bed.getRenderBoundingBox();

		double minX =
				ABB.minX - TileEntityRendererDispatcher.staticPlayerX;
		double minY =
                ABB.minY - TileEntityRendererDispatcher.staticPlayerY;
		double minZ =
                ABB.minZ - TileEntityRendererDispatcher.staticPlayerZ;

        double maxX =
                ABB.maxX - TileEntityRendererDispatcher.staticPlayerX;
        double maxY =
                ABB.maxY - TileEntityRendererDispatcher.staticPlayerY;
        double maxZ =
                ABB.maxZ - TileEntityRendererDispatcher.staticPlayerZ;

		GlStateManager.depthMask(false);
		GlStateManager.disableTexture2D();
		GlStateManager.disableLighting();
		GlStateManager.disableCull();
		GL11.glLineWidth(2f);
		GL11.glEnable(2848);
		GlStateManager.disableDepth();

		RenderUtils.drawOutlinedBoundingBox(new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ), red, green, blue, alpha);
		GlStateManager.enableDepth();
		GlStateManager.enableTexture2D();
		GlStateManager.enableLighting();
		GlStateManager.enableCull();
		GlStateManager.depthMask(true);
	}
}
