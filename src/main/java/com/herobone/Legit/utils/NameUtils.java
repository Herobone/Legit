package com.herobone.Legit.utils;

import com.google.gson.*;
import com.mojang.api.http.BasicHttpClient;
import com.mojang.api.http.HttpBody;
import com.mojang.api.http.HttpClient;
import com.mojang.api.http.HttpHeader;
import com.mojang.api.profiles.HttpProfileRepository;
import com.mojang.api.profiles.Profile;
import com.mojang.api.profiles.ProfileRepository;
import net.minecraft.entity.player.EntityPlayer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class NameUtils {
    private static Gson gson = new Gson();
    private HttpClient client;

    public NameUtils() {
        this(BasicHttpClient.getInstance());
    }

    public NameUtils(HttpClient client) {
        this.client = client;
    }

    public ArrayList<String> getNamesForPlayer(EntityPlayer player) {
        ArrayList<String> result = null;
        try {

            List<HttpHeader> headers = new ArrayList<>();
            headers.add(new HttpHeader("Content-Type", "application/json"));

            ProfileRepository repo = new HttpProfileRepository("minecraft");
            Profile[] profiles = repo.findProfilesByNames(player.getName());

            HttpBody body = getHttpBody();
            result = get(getNamesUrl(profiles[0].getId()), headers);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private URL getNamesUrl(String uniqueID) throws MalformedURLException {
        return new URL("https://api.mojang.com/user/profiles/" + uniqueID + "/names");
    }

    private ArrayList<String> get(URL url, List<HttpHeader> headers) throws IOException {
        String response = client.get(url, null, headers);
        ArrayList<String> list = new ArrayList<>();
        final JsonElement jsonElement = gson.fromJson(response, JsonElement.class);

        if(jsonElement instanceof JsonNull)
            return null;

        final JsonArray jsonArray = (JsonArray) jsonElement;

        for (JsonElement element: jsonArray) {
            JsonObject jsonObject = (JsonObject) element;
            list.add(jsonObject.get("name").getAsString());
        }


        return list;
    }

    private static HttpBody getHttpBody(String... namesBatch) {
        return new HttpBody(gson.toJson(namesBatch));
    }
}
