package com.herobone.Legit.utils;

import com.google.gson.*;
import com.mojang.api.http.BasicHttpClient;
import com.mojang.api.http.HttpBody;
import com.mojang.api.http.HttpClient;
import com.mojang.api.http.HttpHeader;
import com.mojang.api.profiles.HttpProfileRepository;
import com.mojang.api.profiles.Profile;
import com.mojang.api.profiles.ProfileRepository;
import net.minecraft.entity.player.EntityPlayer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CreditUtils {
    private static Gson gson = new Gson();
    private HttpClient client;

    public CreditUtils() {
        this(BasicHttpClient.getInstance());
    }

    public CreditUtils(HttpClient client) {
        this.client = client;
    }

    public JsonElement getCredits() {
        JsonElement result = null;
        try {
            List<HttpHeader> headers = new ArrayList<>();
            headers.add(new HttpHeader("Content-Type", "application/json"));

            result = get(getCreditUrl(), headers);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private URL getCreditUrl() throws MalformedURLException {
        return new URL("https://herobone.github.io/Herobone/credits.json");
    }

    private JsonElement get(URL url, List<HttpHeader> headers) throws IOException {
        String response = client.get(url, null, headers);
        final JsonElement jsonElement = gson.fromJson(response, JsonElement.class);

        if(jsonElement instanceof JsonNull)
            return null;

        /*final JsonArray jsonArray = (JsonArray) jsonElement;

        for (JsonElement element: jsonArray) {
            JsonObject jsonObject = (JsonObject) element;
            list.add(jsonObject.get("name").getAsString());
        }*/


        return jsonElement;
    }
}
