package com.herobone.Legit;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;

import java.lang.reflect.Field;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2018 | Herobone | All rights reserved.
 */
public class Wrapper {

    private static final Minecraft minecraft = Minecraft.getMinecraft();

    public static Minecraft getMinecraft() {
        return minecraft;
    }

    public static Block getBlock(BlockPos pos) {
        return minecraft.world.getBlockState(pos).getBlock();
    }

    public static Block getBlockUnderPlayer(EntityPlayer player) {
        return getBlock(new BlockPos(player.posX, player.posY - 1.0D, player.posZ));
    }

    public static Block getBlockUnderPlayer(EntityPlayer player, double distance) {
        return getBlock(new BlockPos(player.posX, player.posY - distance, player.posZ));
    }
    public static Block getBlockAt(double x, double y, double z) {
        return getBlock(new BlockPos(x, y, z));
    }

    public static double getDistToGround(EntityPlayer player) {
        double distance = 0;
        BlockPos pos =  player.getPosition();
        for (double i = pos.getY(); i >= 0; i--) {
            if (getBlockAt(pos.getX(), i, pos.getZ()) == Blocks.AIR)
                distance++;
            else
                break;
        }
        return distance;
    }

    public static Field getField(Class clazz, String fieldName) throws NoSuchFieldException {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
}