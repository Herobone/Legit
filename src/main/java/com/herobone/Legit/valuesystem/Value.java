package com.herobone.Legit.valuesystem;

import com.herobone.Legit.utils.CallBack;

import java.lang.reflect.InvocationTargetException;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
public class Value<T> {

    private CallBack callback = null;
    private String valueName;
    private T valueObject;

    public Value(String valueName, T valueObject) {
        this.valueName = valueName;
        this.valueObject = valueObject;
    }

    public Value(String valueName, T valueObject, CallBack callback) {
        this.valueName = valueName;
        this.valueObject = valueObject;
        this.callback = callback;
    }

    public String getValueName() {
        return valueName;
    }

    public T getObject() {
        return valueObject;
    }

    public void setObject(T valueObject) {
        this.valueObject = valueObject;
        if (callback != null) {
            try {
                callback.invoke();
            } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
    }
}
