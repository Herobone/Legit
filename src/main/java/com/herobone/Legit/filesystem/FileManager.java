package com.herobone.Legit.filesystem;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.herobone.Legit.LegitBase;
import com.herobone.Legit.Wrapper;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.valuesystem.Value;
import de.Hero.clickgui.ClickGUI;
import de.Hero.clickgui.Panel;
import de.Hero.settings.Setting;
import de.Hero.settings.SettingsManager;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.*;
import java.util.ArrayList;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | Herobone | All rights reserved.
 */
@SideOnly(Side.CLIENT)
public class FileManager {

    private final File dir = new File(Wrapper.getMinecraft().mcDataDir, LegitBase.CLIENT_NAME);

    private final File modules = new File(dir, "modules.json");
    private final File clickgui = new File(dir, "clickgui.json");

    private final Gson gson = new Gson();

    public FileManager() {
        dir.mkdirs();
    }

    public void saveModules() throws IOException {
        if(!modules.exists())
            modules.createNewFile();

        final JsonObject jsonObject = new JsonObject();

        for(final Module module : ModuleManager.getModules()) {
            final JsonObject moduleJson = new JsonObject();

            moduleJson.addProperty("state", module.getState());
            moduleJson.addProperty("key", module.getKeyBind());

            for(final Value value : module.getValues()) {
                if(value.getObject() instanceof Number)
                    moduleJson.addProperty(value.getValueName(), (Number) value.getObject());
                else if(value.getObject() instanceof Boolean)
                    moduleJson.addProperty(value.getValueName(), (Boolean) value.getObject());
                else if(value.getObject() instanceof String)
                    moduleJson.addProperty(value.getValueName(), (String) value.getObject());
            }

            ArrayList<Setting> settingsByMod = LegitBase.setmgr.getSettingsByMod(module);

            if (settingsByMod == null || settingsByMod.size() == 0) {
                jsonObject.add(module.getModuleName(), moduleJson);
                continue;
            }

            for (Setting setting : settingsByMod) {
                if (setting.type.equalsIgnoreCase("value"))
                    continue;

                if (setting.isCheck())
                    moduleJson.addProperty(setting.getName(), setting.getValBoolean());
                else if (setting.isSlider())
                    moduleJson.addProperty(setting.getName(), setting.getValDouble());
                else if (setting.isCombo())
                    moduleJson.addProperty(setting.getName(), setting.getValString());
            }

            jsonObject.add(module.getModuleName(), moduleJson);
        }

        final PrintWriter printWriter = new PrintWriter(modules);
        printWriter.println(gson.toJson(jsonObject));
        printWriter.flush();
        printWriter.close();
    }

    public void loadModules() throws IOException {

        LegitBase.CLIENT_INSTANCE.LOGGER.info(modules);

        if(!modules.exists()) {
            modules.createNewFile();
            saveModules();
            return;
        }

        final BufferedReader bufferedReader = new BufferedReader(new FileReader(modules));

        final JsonElement jsonElement = gson.fromJson(bufferedReader, JsonElement.class);

        if(jsonElement instanceof JsonNull)
            return;

        final JsonObject jsonObject = (JsonObject) jsonElement;

        for(final Module module : ModuleManager.getModules()) {
            if(!jsonObject.has(module.getModuleName()))
                continue;

            final JsonElement moduleElement = jsonObject.get(module.getModuleName());

            if(moduleElement instanceof JsonNull)
                continue;

            final JsonObject moduleJson = (JsonObject) moduleElement;

            if(moduleJson.has("state"))
                module.setState(moduleJson.get("state").getAsBoolean());
            if(moduleJson.has("key"))
             module.setKeyBind(moduleJson.get("key").getAsInt());

            for(final Value value : module.getValues()) {
                if(!moduleJson.has(value.getValueName()))
                    continue;

                if(value.getObject() instanceof Float)
                    value.setObject(moduleJson.get(value.getValueName()).getAsFloat());
                else if(value.getObject() instanceof Double)
                    value.setObject(moduleJson.get(value.getValueName()).getAsDouble());
                else if(value.getObject() instanceof Integer)
                    value.setObject(moduleJson.get(value.getValueName()).getAsInt());
                else if(value.getObject() instanceof Long)
                    value.setObject(moduleJson.get(value.getValueName()).getAsLong());
                else if(value.getObject() instanceof Byte)
                    value.setObject(moduleJson.get(value.getValueName()).getAsByte());
                else if(value.getObject() instanceof Boolean)
                    value.setObject(moduleJson.get(value.getValueName()).getAsBoolean());
                else if(value.getObject() instanceof String)
                    value.setObject(moduleJson.get(value.getValueName()).getAsString());
            }

            ArrayList<Setting> settings = LegitBase.setmgr.getSettingsByMod(module);

            if (settings == null || settings.size() == 0) {
                continue;
            }

            for (Setting setting : settings) {
                if(!moduleJson.has(setting.getName()))
                    continue;

                if (setting.type.equalsIgnoreCase("value"))
                    continue;

                if (setting.isCheck()) {
                    setting.setValBoolean(moduleJson.get(setting.getName()).getAsBoolean());
                } else if (setting.isSlider()) {
                    setting.setValDouble(moduleJson.get(setting.getName()).getAsDouble());
                } else if (setting.isCombo()) {
                    setting.setValString(moduleJson.get(setting.getName()).getAsString());
                }
            }
        }
    }

    public void saveClickGui() throws IOException {
        if(!clickgui.exists())
            clickgui.createNewFile();

        final JsonObject jsonObject = new JsonObject();

        for(Panel panel : ClickGUI.panels) {
            final JsonObject guiJson = new JsonObject();

            guiJson.addProperty("x", panel.x);
            guiJson.addProperty("y", panel.y);
            guiJson.addProperty("extend", panel.extended);

            jsonObject.add(panel.title, guiJson);
            LegitBase.CLIENT_INSTANCE.LOGGER.info("Added " + panel.title + " to JSON");
        }

        final PrintWriter printWriter = new PrintWriter(clickgui);
        printWriter.println(gson.toJson(jsonObject));
        printWriter.flush();
        printWriter.close();
        LegitBase.CLIENT_INSTANCE.LOGGER.info("Saved to JSON");
    }

    public void loadClickGui() throws IOException {

        if(!clickgui.exists()) {
            clickgui.createNewFile();
            saveClickGui();
            return;
        }

        final BufferedReader bufferedReader = new BufferedReader(new FileReader(clickgui));

        final JsonElement jsonElement = gson.fromJson(bufferedReader, JsonElement.class);

        if(jsonElement instanceof JsonNull)
            return;

        final JsonObject jsonObject = (JsonObject) jsonElement;

        for(final Panel panel : ClickGUI.panels) {
            if(!jsonObject.has(panel.title))
                continue;

            final JsonElement panelElement = jsonObject.get(panel.title);

            if(panelElement instanceof JsonNull)
                continue;

            final JsonObject clickGuiJson = (JsonObject) panelElement;

            if(clickGuiJson.has("x"))
                panel.x = clickGuiJson.get("x").getAsDouble();
            if(clickGuiJson.has("y"))
                panel.y = clickGuiJson.get("y").getAsDouble();
            if(clickGuiJson.has("extend"))
                panel.extended = clickGuiJson.get("extend").getAsBoolean();
        }
    }
}