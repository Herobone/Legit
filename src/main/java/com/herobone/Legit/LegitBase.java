package com.herobone.Legit;

import com.herobone.Legit.command.CommandManager;
import com.herobone.Legit.event.EventManager;
import com.herobone.Legit.filesystem.FileManager;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.tracers.ArrowPhysics;
import com.herobone.Legit.tracers.IProjectilePhysics;
import com.herobone.Legit.tracers.ThrowablePhysics;
import de.Hero.clickgui.ClickGUI;
import de.Hero.settings.SettingsManager;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

@SideOnly(Side.CLIENT)
public class LegitBase {

    public static LegitBase CLIENT_INSTANCE;

    public static final String CLIENT_NAME = "Legit";
    public static final int CLIENT_VERSION = 1;
    public static final String CLIENT_AUTHOR = "Herobone";

    public final Logger LOGGER = LogManager.getLogger("Legit");

    public final ModuleManager moduleManager = new ModuleManager();
    public final EventManager eventManager = new EventManager();
    public final CommandManager commandManager = new CommandManager();
    public final FileManager fileManager = new FileManager();

    public static SettingsManager setmgr;
    public static ClickGUI clickGUI;

    public Collection<IProjectilePhysics> projectilePhysics;

    public LegitBase() {
        CLIENT_INSTANCE = this;
    }

    public void startClient() {
        LOGGER.info(String.format("Starting %s b%d by %s", CLIENT_NAME, CLIENT_VERSION, CLIENT_AUTHOR));
        LOGGER.info(CLIENT_NAME + " by " + CLIENT_AUTHOR + " (Copyright 2018)");

        //SettingsMgr zuerst
        setmgr = new SettingsManager();

        commandManager.registerCommands();
        moduleManager.registerModules();
        clickGUI = new ClickGUI();
        eventManager.registerListener(moduleManager);

        try{
            fileManager.loadModules();
            fileManager.loadClickGui();
        }catch(IOException e) {
            e.printStackTrace();
        }

        this.projectilePhysics = new ArrayList();

        this.projectilePhysics.add(new ArrowPhysics());
        this.projectilePhysics.add(new ThrowablePhysics());
    }

    public void stopClient() {
        try{
            fileManager.saveModules();
            fileManager.saveClickGui();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }
}