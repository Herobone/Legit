package com.herobone.Legit.tracers;

import com.herobone.Legit.utils.UsefullStuff;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBow;

public class ArrowPhysics
  extends StandardPhysics
{
  public Item item;
  
  public ArrowPhysics()
  {
    super(0.05D, 0.99D, -0.1D, 0.0D, new Item[] { Items.BOW });
  }
  
  public double launchSpeed(Entity shooter)
  {
    if ((shooter instanceof EntityPlayer))
    {
      EntityPlayer player = (EntityPlayer)shooter;
      int pull = player.getItemInUseCount();
      int max = Items.BOW.getMaxItemUseDuration(player.getHeldItemMainhand());
      return 3.0F * UsefullStuff.getArrowVelocity(pull != 0 ? max - pull : 0);
    }
    return 3.0D;
  }
}
