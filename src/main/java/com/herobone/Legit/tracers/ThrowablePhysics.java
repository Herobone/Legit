package com.herobone.Legit.tracers;

import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class ThrowablePhysics
  extends StandardPhysics
{
  public ThrowablePhysics()
  {
    super(0.03D, 0.99D, -0.1D, 0.0D, new Item[] { Items.SNOWBALL, Items.ENDER_PEARL, Items.EGG });
  }
}
