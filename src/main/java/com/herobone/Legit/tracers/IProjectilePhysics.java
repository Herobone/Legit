package com.herobone.Legit.tracers;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;

public abstract interface IProjectilePhysics
{
  public abstract boolean matchesItem(ItemStack paramItemStack);
  
  public abstract ArrayList<Vec3d> trajectory(Entity paramEntity);
}
