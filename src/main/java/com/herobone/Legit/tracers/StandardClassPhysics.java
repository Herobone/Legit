package com.herobone.Legit.tracers;

import java.util.ArrayList;
import java.util.Collection;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class StandardClassPhysics
  extends StandardPhysics
{
  public Collection<Class> classes = new ArrayList();
  
  public StandardClassPhysics(double grav, double drag, double y, double pitch, Class... items)
  {
    super(grav, drag, y, pitch);
    for (Class c : items) {
      this.classes.add(c);
    }
  }
  
  public boolean matchesItem(ItemStack stack)
  {
    Item item = stack.getItem();
    for (Class c : this.classes) {
      if (c.isInstance(item)) {
        return true;
      }
    }
    return false;
  }
}
