package com.herobone.Legit.tracers;

import java.util.ArrayList;
import java.util.Collection;

import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

public class StandardPhysics
  implements IProjectilePhysics
{
  public static final double d2r = 0.017453292519943295D;
  public static final double r2d = 57.29577951308232D;
  public double gravity;
  public double dragResistance;
  public double yOffset;
  public double pitchOffset;
  public double launchSpeed = 1.5D;
  public Collection<Item> items = new ArrayList();
  
  public StandardPhysics(double grav, double drag, double y)
  {
    this.gravity = grav;
    this.dragResistance = drag;
    this.yOffset = y;
    this.pitchOffset = 0.0D;
  }
  
  public StandardPhysics(double grav, double drag, double y, double pitch)
  {
    this.gravity = grav;
    this.dragResistance = drag;
    this.yOffset = y;
    this.pitchOffset = pitch;
  }
  
  public StandardPhysics(double grav, double drag, double y, double pitch, Item... items)
  {
    this.gravity = grav;
    this.dragResistance = drag;
    this.yOffset = y;
    this.pitchOffset = pitch;
    for (Item i : items) {
      this.items.add(i);
    }
  }
  
  public boolean matchesItem(ItemStack stack)
  {
    return this.items.contains(stack.getItem());
  }
  
  public ArrayList<Vec3d> trajectory(Entity shooter)
  {
    ArrayList<Vec3d> trajectory = new ArrayList(101);
    
    double speedInitialInPlayerFrame = launchSpeed(shooter);
    
    double sinMinusPitch = MathHelper.sin((float)(0.017453292519943295D * (-shooter.rotationPitch - this.pitchOffset)));
    double cosMinusPitch = MathHelper.cos((float)(0.017453292519943295D * -shooter.rotationPitch));
    sinMinusPitch /= MathHelper.sqrt(Math.pow(sinMinusPitch, 2.0D) + Math.pow(cosMinusPitch, 2.0D));
    cosMinusPitch /= MathHelper.sqrt(Math.pow(sinMinusPitch, 2.0D) + Math.pow(cosMinusPitch, 2.0D));
    double sinYaw = MathHelper.sin((float)(0.017453292519943295D * shooter.rotationYaw));
    double cosYaw = MathHelper.cos((float)(0.017453292519943295D * shooter.rotationYaw));
    double factorX = -speedInitialInPlayerFrame * cosMinusPitch * sinYaw + shooter.motionX;
    double factorY = speedInitialInPlayerFrame * sinMinusPitch + (shooter.onGround ? 0.0D : shooter.motionY) + this.gravity / (1.0D - this.dragResistance);
    double factorZ = speedInitialInPlayerFrame * cosMinusPitch * cosYaw + shooter.motionZ;
    
    trajectory.add(new Vec3d(-cosYaw * 0.01D, this.yOffset, -sinYaw * 0.009999999776482582D));
    for (int n = 0; n < 100; n++)
    {
      double dragFactor = (1.0D - Math.pow(this.dragResistance, n)) / (1.0D - this.dragResistance);
      trajectory.add(new Vec3d(factorX * dragFactor, factorY * dragFactor - this.gravity / (1.0D - this.dragResistance) * n, factorZ * dragFactor).add((Vec3d)trajectory.get(0)));
    }
    return trajectory;
  }
  
  public double launchSpeed(Entity shooter)
  {
    return this.launchSpeed;
  }
  
  public void setLaunchSpeed(double speed)
  {
    this.launchSpeed = speed;
  }
}
