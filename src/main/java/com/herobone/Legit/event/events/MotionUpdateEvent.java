package com.herobone.Legit.event.events;

import com.herobone.Legit.event.Event;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
public class MotionUpdateEvent extends Event {

    private State state;

    public MotionUpdateEvent(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public enum State {
        PRE, POST;
    }
}