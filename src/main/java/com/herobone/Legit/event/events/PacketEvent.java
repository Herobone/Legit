package com.herobone.Legit.event.events;

import com.herobone.Legit.event.CancellableEvent;
import net.minecraft.network.Packet;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
public class PacketEvent extends CancellableEvent {

    private Packet packet;

    public PacketEvent(Packet packet) {
        this.packet = packet;
    }

    public Packet getPacket() {
        return packet;
    }
}