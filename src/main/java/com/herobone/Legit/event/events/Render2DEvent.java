package com.herobone.Legit.event.events;

import com.herobone.Legit.event.Event;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
public class Render2DEvent extends Event {

    private State state;

    public Render2DEvent(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public enum State {
        PRE, POST;
    }
}