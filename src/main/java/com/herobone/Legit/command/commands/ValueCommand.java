package com.herobone.Legit.command.commands;

import com.herobone.Legit.command.Command;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.utils.ChatUtils;
import com.herobone.Legit.valuesystem.Value;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
public class ValueCommand extends Command {

    public ValueCommand() {
        super("value");
    }

    @Override
    public void execute(String[] strings) {
        if(strings.length > 3) {
            final Module module = ModuleManager.getModule(strings[1]);

            if(module == null) {
                ChatUtils.displayMessage("§c§lError: §r§aThe entered module not exist.");
                return;
            }

            final Value value = module.getValue(strings[2]);

            if(value == null) {
                ChatUtils.displayMessage("§c§lError: §r§aThe entered value not exist.");
                return;
            }

            if(value.getObject() instanceof Float) {
                final float newValue = Float.parseFloat(strings[3]);
                value.setObject(newValue);
                ChatUtils.displayMessage("§cThe value of §a§l" + module.getModuleName() + " §8(§a§l" + value.getValueName() + ") §c was set to §a§l" + newValue + "§c.");
            } else if(value.getObject() instanceof Integer) {
                final float newValue = Integer.parseInt(strings[3]);
                value.setObject(newValue);
                ChatUtils.displayMessage("§cThe value of §a§l" + module.getModuleName() + " §8(§a§l" + value.getValueName() + ") §c was set to §a§l" + newValue + "§c.");
            } else if(value.getObject() instanceof Boolean) {
                final boolean newValue = Boolean.parseBoolean(strings[3]);
                value.setObject(newValue);
                ChatUtils.displayMessage("§cThe value of §a§l" + module.getModuleName() + " §8(§a§l" + value.getValueName() + ") §c was set to §a§l" + newValue + "§c.");
            } else {
                value.setObject(strings[3]);
                ChatUtils.displayMessage("§cThe value of §a§l" + module.getModuleName() + " §8(§a§l" + value.getValueName() + ") §c was set to §a§l" + strings[3] + "§c.");
            }
            return;
        }

        if(strings.length > 2) {
            final Module module = ModuleManager.getModule(strings[1]);

            if(module == null) {
                ChatUtils.displayMessage("§c§lError: §r§aThe entered module not exist.");
                return;
            }

            final Value value = module.getValue(strings[2]);

            if(value == null) {
                ChatUtils.displayMessage("§c§lError: §r§aThe entered value not exist.");
                return;
            }

            ChatUtils.displayMessage("§cThe value of §a§l" + module.getModuleName() + " §8(§a§l" + value.getValueName() + ") §c is §a§l" + value.getObject() + "§c.");
        }

        ChatUtils.displayMessage("§c§lSyntax: §r§a.value <module> <valuename> <new value>");
    }
}