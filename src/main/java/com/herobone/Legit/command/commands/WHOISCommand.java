package com.herobone.Legit.command.commands;

import com.herobone.Legit.command.Command;
import com.herobone.Legit.utils.ChatUtils;
import com.herobone.Legit.utils.NameUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

import java.util.ArrayList;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
public class WHOISCommand extends Command {

    public EntityPlayer player = mc.player;

    public WHOISCommand() {
        super("whois");
    }

    @Override
    public void execute(String[] strings) {
        if(strings.length > 1) {
            for (Object obj : Minecraft.getMinecraft().world.loadedEntityList) {
                if (obj instanceof EntityPlayer) {
                    if (((EntityPlayer) obj).getName().equals(strings[1])) {
                        player = (EntityPlayer) obj;
                    }
                }
            }
            if (player == null) {
                ChatUtils.displayMessage("§cPlayer §7" + strings[1] + "§c not found or not loaded");
                return;
            } else {
                NameUtils names = new NameUtils();
                ArrayList<String> namesList = names.getNamesForPlayer(player);
                StringBuilder prevNames = new StringBuilder();
                ChatUtils.displayMessage("§cPlayer §a§l" + player.getName() + "§c has UUID §a§l" + player.getUniqueID().toString());
                for (String str : namesList) {
                    prevNames.append(str + " ");
                }
                ChatUtils.displayMessage("§cPlayer §a§l" + player.getName() + "§c had these §8" + namesList.size() + "§c names: §a§l" + prevNames.toString());
                return;
            }
        }

        ChatUtils.displayMessage("§c§lSyntax: §r§a.whois <player>");
    }
}