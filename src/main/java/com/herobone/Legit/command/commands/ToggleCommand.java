package com.herobone.Legit.command.commands;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.command.Command;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.utils.ChatUtils;

/**
 * Copyright © 2015 - 2017 | CCBlueX | All rights reserved.
 * <p>
 * LegitBase - By CCBlueX(Marco)
 */
public class ToggleCommand extends Command {

    public ToggleCommand() {
        super("t");
    }

    @Override
    public void execute(String[] strings) {
        if(strings.length > 1) {
            final Module module = ModuleManager.getModule(strings[1]);

            if(module == null) {
                LegitBase.CLIENT_INSTANCE.LOGGER.info("Command called Stage: Module Not Existing");
                ChatUtils.displayMessage("§c§lError: §r§aThe entered module not exist.");
                return;
            }

            module.setState(!module.getState());
            LegitBase.CLIENT_INSTANCE.LOGGER.info("Command called Stage: Command Toggled");
            ChatUtils.displayMessage("§cToggled module §7" + module.getModuleName());
            return;
        }

        LegitBase.CLIENT_INSTANCE.LOGGER.info("Command called Stage: Syntax Error");
        ChatUtils.displayMessage("§c§lSyntax: §r§a.t <module>");
    }
}
