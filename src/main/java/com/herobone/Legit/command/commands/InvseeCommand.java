package com.herobone.Legit.command.commands;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.command.Command;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.module.modules.player.Invsee;
import com.herobone.Legit.module.modules.render.ClickGUI;
import com.herobone.Legit.utils.ChatUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;

/**
 * Copyright © 2015 - 2017 | CCBlueX | All rights reserved.
 * <p>
 * LegitBase - By CCBlueX(Marco)
 */
public class InvseeCommand extends Command {

    public EntityPlayer player = mc.player;
    public static InvseeCommand instance;

    public InvseeCommand() {
        super("invsee");
        instance = this;
    }

    @Override
    public void execute(String[] strings) {
        if(strings.length > 1) {
            for (Object obj : Minecraft.getMinecraft().world.loadedEntityList) {
                if (obj instanceof EntityPlayer) {
                    if (((EntityPlayer) obj).getName().equals(strings[1])) {
                        player = (EntityPlayer) obj;
                    }
                }
            }
            if (player == null) {
                ChatUtils.displayMessage("§cPlayer §7" + strings[1] + "§c not found or not loaded");
                return;
            } else {
                ((Invsee)ModuleManager.getModule(Invsee.class)).setState(true);
                ChatUtils.displayMessage("§cShowing inventory of §7" + player);
                return;
            }
        }
        ChatUtils.displayMessage("§c§lSyntax: §r§a.invsee <player>");
    }
}
