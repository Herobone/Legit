package com.herobone.Legit.command.commands;

import com.herobone.Legit.command.Command;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.utils.ChatUtils;
import org.lwjgl.input.Keyboard;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
public class BindCommand extends Command {

    public BindCommand() {
        super("bind");
    }

    @Override
    public void execute(String[] strings) {
        if(strings.length > 2) {
            final Module module = ModuleManager.getModule(strings[1]);

            if(module == null) {
                ChatUtils.displayMessage("§c§lError: §r§aThe entered module not exist.");
                return;
            }

            final int key = Keyboard.getKeyIndex(strings[2].toUpperCase());

            module.setKeyBind(key);
            ChatUtils.displayMessage("§cThe keybind of §a§l" + module.getModuleName() + " §r§cwas set to §a§l" + Keyboard.getKeyName(key) + "§c.");
            return;
        }

        ChatUtils.displayMessage("§c§lSyntax: §r§a.bind <module> <key>");
    }
}