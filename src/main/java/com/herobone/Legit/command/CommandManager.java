package com.herobone.Legit.command;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.command.commands.*;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@SideOnly(Side.CLIENT)
public class CommandManager {

    private final List<Command> commands = new ArrayList<>();

    public void registerCommands() {
        registerCommand(new BindCommand());
        registerCommand(new ValueCommand());
        registerCommand(new ToggleCommand());
        registerCommand(new InvseeCommand());
        registerCommand(new WHOISCommand());
    }

    public void registerCommand(final Command command) {
        commands.add(command);
    }

    public void callCommand(final String s) {
        final String[] strings = s.split(" ");
        for (int i = 0; i < commands.size(); i++) {
            Command command = commands.get(i);
            if (("." + command.getName()).equals(strings[0])) {
                LegitBase.CLIENT_INSTANCE.LOGGER.info("Command called "+ command.getName());
                command.execute(strings);
            }
        }
        //commands.stream().filter(command -> strings[0].equalsIgnoreCase("?" + command.getName())).forEach(command -> command.execute(strings));
    }
}