package com.herobone.Legit.injection.mixins;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.gui.GuiCredits;
import com.herobone.Legit.utils.RenderUtils;
import de.vitox.particle.Particle;
import de.vitox.particle.ParticleGenerator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Random;

@Mixin(GuiMainMenu.class)
public class MixinGuiMainMenu extends GuiScreen implements GuiYesNoCallback {

    private ParticleGenerator particles;
    private final Random random = new Random();

    @Shadow private GuiButton realmsButton;
    @Shadow @Final private Object threadLock;
    @Shadow private int openGLWarning2Width;
    @Shadow private int openGLWarning1Width;
    @Shadow private int openGLWarningX1;
    @Shadow private int openGLWarningY1;
    @Shadow private int openGLWarningX2;
    @Shadow private int openGLWarningY2;
    @Shadow private String openGLWarning1;
    @Shadow private String openGLWarning2;

    FontRenderer renderer = Minecraft.getMinecraft().fontRenderer;

    @Inject(method = "actionPerformed", at = @At("RETURN"))
    public void actionInject(GuiButton button, CallbackInfo callbackInfo) {
        if (button.id == 123)
        {
            this.mc.displayGuiScreen(new GuiCredits(this));
        }
    }

    @Override
    public void initGui() {
        addSingleplayerMultiplayerButtons();

        this.buttonList.add(new GuiButton(0, 24, 24 + 24 * 4, renderer.getStringWidth(I18n.format("menu.options")) + 10,20, I18n.format("menu.options")));
        this.buttonList.add(new GuiButton(123, 24, 24 + 24 * 5, renderer.getStringWidth("Credits") + 10,20, "Credits"));
        this.buttonList.add(new GuiButton(4, 24, 24 + 24 * 6, renderer.getStringWidth(I18n.format("menu.quit")) + 10,20, I18n.format("menu.quit")));
        this.buttonList.add(new GuiButtonLanguage(5, 24, 24 + 24 * 7));

        synchronized (this.threadLock)
        {
            this.openGLWarning1Width = this.fontRenderer.getStringWidth(this.openGLWarning1);
            this.openGLWarning2Width = this.fontRenderer.getStringWidth(this.openGLWarning2);
            int k = Math.max(this.openGLWarning1Width, this.openGLWarning2Width);
            this.openGLWarningX1 = (this.width - k) / 2;
            this.openGLWarningY1 = (this.buttonList.get(0)).y - 24;
            this.openGLWarningX2 = this.openGLWarningX1 + k;
            this.openGLWarningY2 = this.openGLWarningY1 + 24;
        }

        this.mc.setConnectedToRealms(false);

        this.particles = new ParticleGenerator(100, this.width, this.height);
    }

    private void addSingleplayerMultiplayerButtons()
    {
        this.buttonList.add(new GuiButton(1, 24,24, renderer.getStringWidth(I18n.format("menu.singleplayer")) + 10,20, I18n.format("menu.singleplayer")));
        this.buttonList.add(new GuiButton(2, 24, 24 + 24,renderer.getStringWidth(I18n.format("menu.multiplayer")) + 10,20, I18n.format("menu.multiplayer")));
        this.realmsButton = this.addButton(new GuiButton(14, 24, 24 + 24 * 2, renderer.getStringWidth(I18n.format("menu.online").replace("Minecraft", "").trim()) + 10,20, I18n.format("menu.online").replace("Minecraft", "").trim()));
        this.buttonList.add(new GuiButton(6, 24, 24 + 24 * 3, renderer.getStringWidth(I18n.format("fml.menu.mods")) + 10,20, I18n.format("fml.menu.mods")));
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        ScaledResolution resolution = new ScaledResolution(this.mc);

        this.mc.getTextureManager().bindTexture(new ResourceLocation("legit/bg.jpg"));
        Gui.drawModalRectWithCustomSizedTexture(0,0,0,0,resolution.getScaledWidth(),resolution.getScaledHeight(),resolution.getScaledWidth(),resolution.getScaledHeight());
        Gui.drawRect(0,0,width, height, 0x40000000);
        float scale = 3.5f;

        GlStateManager.scale(scale, scale, scale);

        renderer.drawStringWithShadow(LegitBase.CLIENT_NAME, resolution.getScaledWidth() / (scale * 2) - (renderer.getStringWidth(LegitBase.CLIENT_NAME) / 2), 24 / scale, RenderUtils.rainbowEffect(2000).getRGB());

        GlStateManager.scale(1 / scale, 1 / scale, 1 / scale);

        drawParticlesBG(mouseX, mouseY);

        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    private void drawParticlesBG(int mouseX, int mouseY) {
        for (Particle p : particles.particles) {
            for (Particle p2 : particles.particles) {
                int xx = (int) (MathHelper.cos(0.1F * (p.x + p.k)) * 10.0F);
                int xx2 = (int) (MathHelper.cos(0.1F * (p2.x + p2.k)) * 10.0F);

                boolean mouseOver = (mouseX >= p.x + xx - 95) && (mouseY >= p.y - 90) && (mouseX <= p.x)
                        && (mouseY <= p.y);

                if (mouseOver) {
                    if (mouseY >= p.y - 80 && mouseX >= p2.x - 100 && mouseY >= p2.y && mouseY <= p2.y + 70
                            && mouseX <= p2.x) {

                        int maxDistance = 100;

                        final int xDif = p.x - mouseX;
                        final int yDif = p.y - mouseY;
                        final int distance = (int) Math.sqrt(xDif * xDif + yDif + yDif);

                        final int xDif1 = p2.x - mouseX;
                        final int yDif1 = p2.y - mouseY;
                        final int distance2 = (int) Math.sqrt(xDif1 * xDif1 + yDif1 + yDif1);

                        if (distance < maxDistance && distance2 < maxDistance) {

                            GL11.glPushMatrix();
                            GL11.glEnable(GL11.GL_LINE_SMOOTH);
                            GL11.glDisable(GL11.GL_DEPTH_TEST);
                            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                            GL11.glDisable(GL11.GL_TEXTURE_2D);
                            GL11.glDepthMask(false);
                            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                            GL11.glEnable(GL11.GL_BLEND);
                            GL11.glLineWidth(1.5F);
                            GL11.glBegin(GL11.GL_LINES);

                            GL11.glVertex2d(p.x + xx, p.y);
                            GL11.glVertex2d(p2.x + xx2, p2.y);
                            GL11.glEnd();
                            GL11.glPopMatrix();

                            if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
                                if (p2.x > mouseX) {
                                    p2.y -= random.nextInt(5);
                                }
                                if (p2.y < mouseY) {
                                    p2.x += random.nextInt(5);
                                }

                            }

                        }
                    }
                }
            }

        }

        this.particles.drawParticles();
    }

}
