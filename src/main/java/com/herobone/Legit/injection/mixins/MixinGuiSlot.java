package com.herobone.Legit.injection.mixins;

import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiSlot;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.awt.*;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@SideOnly(Side.CLIENT)
@Mixin(GuiSlot.class)
public class MixinGuiSlot {

    @Shadow
    public int top;
    @Shadow
    public int bottom;
    @Shadow
    public int right;
    @Shadow
    public int left;
    @Shadow
    public int width;
    @Shadow
    public int height;

    protected void drawContainerBackground(Tessellator tessellator) {
        Gui.drawRect(this.left, this.top, this.right, this.bottom, new Color(0, 0, 0, 123).getRGB());
    }

    protected void overlayBackground(int startY, int endY, int startAlpha, int endAlpha) {
        int color = new Color(0, 0, 0, 166).getRGB();
        float f3 = (float) (color >> 24 & 255) / 255.0F;
        float f = (float) (color >> 16 & 255) / 255.0F;
        float f1 = (float) (color >> 8 & 255) / 255.0F;
        float f2 = (float) (color & 255) / 255.0F;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        GlStateManager.color(f, f1, f2, f3);
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION);
        bufferbuilder.pos((double) this.left, (double) endY, 0.0D).endVertex();
        bufferbuilder.pos((double) (this.left + this.width), (double) endY, 0.0D).endVertex();
        bufferbuilder.pos((double) (this.left + this.width), (double) startY, 0.0D).endVertex();
        bufferbuilder.pos((double) this.left, (double) startY, 0.0D).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
    }

}