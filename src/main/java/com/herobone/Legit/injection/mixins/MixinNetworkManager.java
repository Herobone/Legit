package com.herobone.Legit.injection.mixins;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import com.herobone.Legit.event.EventManager;
import com.herobone.Legit.event.events.PacketEvent;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.client.CPacketCustomPayload;
import net.minecraftforge.fml.common.network.internal.FMLProxyPacket;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.lang.reflect.Field;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@Mixin(NetworkManager.class)
public class MixinNetworkManager {

    @Inject(method = "channelRead0", at = @At(value = "INVOKE", target = "Lnet/minecraft/network/Packet;processPacket(Lnet/minecraft/network/INetHandler;)V"), cancellable = true)
    private void readPacket(ChannelHandlerContext p_channelRead0_1_, Packet p_channelRead0_2_, CallbackInfo callbackInfo) {
        final PacketEvent packetEvent = new PacketEvent(p_channelRead0_2_);
        EventManager.callEvent(packetEvent);

        if(packetEvent.isCancelled())
            callbackInfo.cancel();
    }

    @Inject(method = "sendPacket(Lnet/minecraft/network/Packet;)V", at = @At("HEAD"), cancellable = true)
    private void sendPacket(Packet packetIn, CallbackInfo callbackInfo) {
        final PacketEvent packetEvent = new PacketEvent(packetIn);
        EventManager.callEvent(packetEvent);

        if(packetEvent.isCancelled())
            callbackInfo.cancel();
    }

    @Inject(method = "sendPacket(Lnet/minecraft/network/Packet;)V", at = @At("HEAD"), cancellable = true)
    private void sendPacketNoForge(final Packet<?> packet, final CallbackInfo callbackInfo) {
        if(packet instanceof FMLProxyPacket) {
            callbackInfo.cancel();
            return;
        }

        if(packet instanceof CPacketCustomPayload) {
            final CPacketCustomPayload packetCustomPayload = (CPacketCustomPayload) packet;
            final String channelName = packetCustomPayload.getChannelName();

            if(!channelName.startsWith("MC|"))
                callbackInfo.cancel();
            else if(channelName.equalsIgnoreCase("MC|Brand")) {
                final PacketBuffer packetBuffer = new PacketBuffer(Unpooled.buffer()).writeString("vanilla");

                try{
                    final Field field = packetCustomPayload.getClass().getDeclaredField("field_149561_c");
                    field.setAccessible(true);
                    field.set(packetCustomPayload, packetBuffer);
                }catch(final NoSuchFieldException e) {
                    try {
                        final Field field = packetCustomPayload.getClass().getDeclaredField("data");
                        field.setAccessible(true);
                        field.set(packetCustomPayload, packetBuffer);
                    }catch(NoSuchFieldException | IllegalAccessException e1) {
                        e1.printStackTrace();
                    }
                }catch(IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}