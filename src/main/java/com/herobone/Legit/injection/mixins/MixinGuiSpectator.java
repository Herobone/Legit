package com.herobone.Legit.injection.mixins;

import com.herobone.Legit.event.EventManager;
import com.herobone.Legit.event.events.Render2DEvent;
import net.minecraft.client.gui.GuiSpectator;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@Mixin(GuiSpectator.class)
public class MixinGuiSpectator {

    @Inject(method = "renderTooltip", at = @At("HEAD"))
    private void postRender2D(CallbackInfo callbackInfo) {
        EventManager.callEvent(new Render2DEvent(Render2DEvent.State.PRE));
    }

    @Inject(method = "renderTooltip", at = @At("RETURN"))
    private void preRender2D(CallbackInfo callbackInfo) {
        EventManager.callEvent(new Render2DEvent(Render2DEvent.State.POST));
    }
}