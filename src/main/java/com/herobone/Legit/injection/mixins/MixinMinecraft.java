package com.herobone.Legit.injection.mixins;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.event.EventManager;
import com.herobone.Legit.event.events.KeyEvent;
import com.herobone.Legit.interfaces.IMixinMinecraft;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.block.model.ModelManager;
import net.minecraft.util.Timer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@Mixin(Minecraft.class)
@SideOnly(Side.CLIENT)
public class MixinMinecraft implements IMixinMinecraft {

    @Shadow public GuiScreen currentScreen;
    @Shadow private ModelManager modelManager;
    @Shadow @Final private Timer timer;

    @Shadow
    private void clickMouse() {
    }

    @Shadow
    private void rightClickMouse() {
    }

    @Inject(method = "<init>", at = @At("RETURN"))
    private void initMinecraft(CallbackInfo callbackInfo) {
        new LegitBase();
    }

    @Inject(method = "init", at = @At(value = "FIELD", target = "Lnet/minecraft/client/Minecraft;ingameGUI:Lnet/minecraft/client/gui/GuiIngame;", shift = At.Shift.AFTER))
    private void startClient(CallbackInfo callbackInfo) {
        LegitBase.CLIENT_INSTANCE.startClient();
    }

    @Inject(method = "shutdown", at = @At("HEAD"))
    private void stopClient(CallbackInfo callbackInfo) {
        LegitBase.CLIENT_INSTANCE.stopClient();
    }

    @Inject(method = "runTickKeyboard", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/Minecraft;dispatchKeypresses()V", shift = At.Shift.AFTER))
    private void keyPress(CallbackInfo callbackInfo) {
        if(Keyboard.getEventKeyState() && currentScreen == null)
            EventManager.callEvent(new KeyEvent(Keyboard.getEventKey() == 0 ? Keyboard.getEventCharacter() + 256 : Keyboard.getEventKey()));
    }


    @Override
    public ModelManager getModelManager() {
        return modelManager;
    }

    @Override
    public Timer getTimer() {
        return timer;
    }

    @Override
    public void click(String button) {
        if (button.equalsIgnoreCase("left")) {
            this.clickMouse();
        } else if (button.equalsIgnoreCase("right")) {
            this.rightClickMouse();
        }
    }
}