package com.herobone.Legit.injection.mixins;

import com.herobone.Legit.LegitBase;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@SideOnly(Side.CLIENT)
@Mixin(GuiScreen.class)
public class MixinGuiScreen extends Gui {

    @Shadow public Minecraft mc;

    @Shadow public int height;
    @Shadow int width;

    @Inject(method = "sendChatMessage(Ljava/lang/String;Z)V", at = @At("HEAD"), cancellable = true)
    private void chatMessage(String msg, boolean addToChat, CallbackInfo callbackInfo) {
        if(msg.startsWith(".")) {
            LegitBase.CLIENT_INSTANCE.commandManager.callCommand(msg);
            LegitBase.CLIENT_INSTANCE.LOGGER.info("Command called");
            mc.ingameGUI.getChatGUI().addToSentMessages(msg);
            callbackInfo.cancel();
        }
    }

    public void drawBackground(int tint)
    {
        ScaledResolution resolution = new ScaledResolution(this.mc);

        this.mc.getTextureManager().bindTexture(new ResourceLocation("legit/bg.jpg"));
        Gui.drawModalRectWithCustomSizedTexture(0,0,0,0,resolution.getScaledWidth(),resolution.getScaledHeight(),resolution.getScaledWidth(),resolution.getScaledHeight());
        Gui.drawRect(0,0,width, height, 0x40000000);
    }
}