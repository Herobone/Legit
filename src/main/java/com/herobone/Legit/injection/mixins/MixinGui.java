package com.herobone.Legit.injection.mixins;

import net.minecraft.client.gui.Gui;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@SideOnly(Side.CLIENT)
@Mixin(Gui.class)
public class MixinGui {
    @Shadow @Final @Mutable
    public static ResourceLocation OPTIONS_BACKGROUND;

    static {

        //OPTIONS_BACKGROUND = new ResourceLocation("legit/blacktranslbg.png");
    }

}