package com.herobone.Legit.injection.mixins;

import com.herobone.Legit.event.EventManager;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.event.events.MotionUpdateEvent;
import net.minecraft.client.entity.EntityPlayerSP;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin(EntityPlayerSP.class)
public class MixinEntityPlayerSP {

    @Inject(method = "onUpdateWalkingPlayer", at = @At("HEAD"))
    private void preMotion(CallbackInfo callbackInfo) {
        EventManager.callEvent(new MotionUpdateEvent(MotionUpdateEvent.State.PRE));
    }

    @Inject(method = "onUpdateWalkingPlayer", at = @At("RETURN"))
    private void postMotion(CallbackInfo callbackInfo) {
        EventManager.callEvent(new MotionUpdateEvent(MotionUpdateEvent.State.POST));
    }

    @Inject(method = "onUpdate", at = @At("HEAD"))
    private void onUpdate(CallbackInfo callbackInfo) {
        EventManager.callEvent(new EventUpdate());
    }
}