package com.herobone.Legit.injection.mixins;

import com.herobone.Legit.event.EventManager;
import com.herobone.Legit.event.events.DoRenderEvent;
import com.herobone.Legit.interfaces.IMixinRenderManager;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(RenderManager.class)
public abstract class MixinRenderManager implements IMixinRenderManager {

    @Shadow private double renderPosX;
    @Shadow private double renderPosY;
    @Shadow private double renderPosZ;

    @Override
    public BlockPos getRenderManagerPos() {
        return new BlockPos(renderPosX,renderPosY,renderPosZ);
    }

}
