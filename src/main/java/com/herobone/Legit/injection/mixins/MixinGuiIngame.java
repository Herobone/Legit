package com.herobone.Legit.injection.mixins;

import com.herobone.Legit.event.EventManager;
import com.herobone.Legit.event.events.Render2DEvent;
import net.minecraft.client.gui.GuiIngame;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@Mixin(GuiIngame.class)
public class MixinGuiIngame {

    @Inject(method = "renderHotbar", at = @At("HEAD"))
    private void postRender2D(CallbackInfo callbackInfo) {
        EventManager.callEvent(new Render2DEvent(Render2DEvent.State.PRE));
    }

    @Inject(method = "renderHotbar", at = @At("RETURN"))
    private void preRender2D(CallbackInfo callbackInfo) {
        EventManager.callEvent(new Render2DEvent(Render2DEvent.State.POST));
    }
}