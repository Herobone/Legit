package com.herobone.Legit.injection.mixins;

import com.herobone.Legit.event.EventManager;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.event.events.MotionUpdateEvent;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin(Entity.class)
public class MixinEntity {

    @Inject(method = "onUpdate", at = @At("HEAD"))
    private void onUpdate(CallbackInfo callbackInfo) {
        EventManager.callEvent(new EventUpdate());
    }
}