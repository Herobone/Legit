package com.herobone.Legit.interfaces;

import net.minecraft.client.gui.FontRenderer;

public interface IMixinEntityRenderer {

    static void drawNameplate(FontRenderer fontRendererIn, String str, float x, float y, float z, int verticalShift, float viewerYaw, float viewerPitch, boolean isThirdPersonFrontal, boolean isSneaking, float scale) {}

}
