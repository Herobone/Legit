package com.herobone.Legit.interfaces;

import net.minecraft.util.math.BlockPos;

public interface IMixinRenderManager {

    BlockPos getRenderManagerPos();

}
