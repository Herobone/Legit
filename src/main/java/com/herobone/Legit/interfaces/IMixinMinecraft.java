package com.herobone.Legit.interfaces;

import net.minecraft.client.renderer.block.model.ModelManager;
import net.minecraft.util.Timer;

public interface IMixinMinecraft {

    ModelManager getModelManager();

    Timer getTimer();

    void click(String button);

}
