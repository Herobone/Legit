package com.herobone.Legit.gui;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.herobone.Legit.LegitBase;
import com.herobone.Legit.utils.CreditUtils;
import de.vitox.particle.Particle;
import de.vitox.particle.ParticleGenerator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.client.GuiScrollingList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class GuiCredits extends GuiScreen {

    private FontRenderer renderer = Minecraft.getMinecraft().fontRenderer;
    private final GuiScreen lastScreen;
    private ParticleGenerator particles;
    private Random random = new Random();
    private List list;
    private Info info;
    private int selectedCredit = 0;
    java.util.List<ITextComponent> lines;

    public GuiCredits(GuiScreen lastScreen) {
        this.lastScreen = lastScreen;
    }

    @Override
    public void initGui() {
        this.particles = new ParticleGenerator(50, this.width, this.height);
        this.buttonList.add(new GuiButton(1, 24, height - 44, renderer.getStringWidth(I18n.format("menu.quit")) + 10,20, I18n.format("gui.done")));
        this.list = new GuiCredits.List(this.mc);
        this.list.registerScrollButtons(7, 8);
        super.initGui();
        update();
    }

    protected void actionPerformed(GuiButton button) {
        if (button.enabled)
        {
            if (button.id == 1)
            {
                this.mc.displayGuiScreen(this.lastScreen);
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        ScaledResolution resolution = new ScaledResolution(this.mc);

        this.mc.getTextureManager().bindTexture(new ResourceLocation("legit/bg.jpg"));
        Gui.drawModalRectWithCustomSizedTexture(0,0,0,0,resolution.getScaledWidth(),resolution.getScaledHeight(),resolution.getScaledWidth(),resolution.getScaledHeight());
        Gui.drawRect(0,0,width, height, 0x40000000);

        drawParticlesBG(mouseX, mouseY);

        this.list.drawScreen(mouseX, mouseY, partialTicks);

        if (this.info != null)
            this.info.drawScreen(mouseX, mouseY, partialTicks);

        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    protected void update() {
        LegitBase.CLIENT_INSTANCE.LOGGER.info("Updating Screen...");
        info = null;
        java.util.List<String> stringLines = new ArrayList<String>();

        stringLines.add("§2§l§n" + list.creditList.get(selectedCredit));

        JsonElement element = list.jsonArray.get(selectedCredit);
        JsonObject jsonObject = (JsonObject) element;

        if (jsonObject.has("DiscordChannel")) {
            stringLines.add("§a§lDiscord Channel:§r " + jsonObject.get("DiscordChannel").getAsString() + " " + jsonObject.get("DiscordUrl").getAsString());
        }
        if (jsonObject.has("YouTubeName")) {
            stringLines.add("§a§lYouTube:§r " + jsonObject.get("YouTubeName").getAsString() + " " + jsonObject.get("YouTubeURL").getAsString());
        }
        if (jsonObject.has("TwitterName")) {
            stringLines.add("§a§lTwitter:§r " + jsonObject.get("TwitterName").getAsString() + " " + jsonObject.get("TwitterURL").getAsString());
        }

        JsonArray array = jsonObject.get("Credits").getAsJsonArray();
        if (array.size() != 0) {
            stringLines.add(null);
            stringLines.add("§eCredits:");
            for (JsonElement jsonElement:array) {
                JsonObject object = (JsonObject) jsonElement;
                stringLines.add(" - " + object.get("credit").getAsString());
            }
        }

        info = new Info(this.width - this.list.width - 30, stringLines);
        LegitBase.CLIENT_INSTANCE.LOGGER.info("Finished!");
        LegitBase.CLIENT_INSTANCE.LOGGER.info(lines);
    }

    @Override
    public void handleMouseInput() throws IOException {
        this.list.handleMouseInput();
        int mouseX = Mouse.getEventX() * this.width / this.mc.displayWidth;
        int mouseY = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;

        if (this.info != null)
            this.info.handleMouseInput(mouseX, mouseY);
        super.handleMouseInput();
    }

    private void drawParticlesBG(int mouseX, int mouseY) {
        for (Particle p : particles.particles) {
            for (Particle p2 : particles.particles) {
                int xx = (int) (MathHelper.cos(0.1F * (p.x + p.k)) * 10.0F);
                int xx2 = (int) (MathHelper.cos(0.1F * (p2.x + p2.k)) * 10.0F);

                boolean mouseOver = (mouseX >= p.x + xx - 95) && (mouseY >= p.y - 90) && (mouseX <= p.x)
                        && (mouseY <= p.y);

                if (mouseOver) {
                    if (mouseY >= p.y - 80 && mouseX >= p2.x - 100 && mouseY >= p2.y && mouseY <= p2.y + 70
                            && mouseX <= p2.x) {

                        int maxDistance = 100;

                        final int xDif = p.x - mouseX;
                        final int yDif = p.y - mouseY;
                        final int distance = (int) Math.sqrt(xDif * xDif + yDif + yDif);

                        final int xDif1 = p2.x - mouseX;
                        final int yDif1 = p2.y - mouseY;
                        final int distance2 = (int) Math.sqrt(xDif1 * xDif1 + yDif1 + yDif1);

                        if (distance < maxDistance && distance2 < maxDistance) {

                            GL11.glPushMatrix();
                            GL11.glEnable(GL11.GL_LINE_SMOOTH);
                            GL11.glDisable(GL11.GL_DEPTH_TEST);
                            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                            GL11.glDisable(GL11.GL_TEXTURE_2D);
                            GL11.glDepthMask(false);
                            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                            GL11.glEnable(GL11.GL_BLEND);
                            GL11.glLineWidth(1.5F);
                            GL11.glBegin(GL11.GL_LINES);

                            GL11.glVertex2d(p.x + xx, p.y);
                            GL11.glVertex2d(p2.x + xx2, p2.y);
                            GL11.glEnd();
                            GL11.glPopMatrix();

                            if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
                                if (p2.x > mouseX) {
                                    p2.y -= random.nextInt(5);
                                }
                                if (p2.y < mouseY) {
                                    p2.x += random.nextInt(5);
                                }

                            }

                        }
                    }
                }
            }

        }

        this.particles.drawParticles();
    }

    @SideOnly(Side.CLIENT)
    class List extends GuiSlot {
        /** A list containing the many different locale language codes. */
        protected final java.util.List<String> creditList = Lists.newArrayList();
        CreditUtils creditUtils = new CreditUtils();
        final JsonElement jsonElement = creditUtils.getCredits();
        final JsonArray jsonArray = (JsonArray) jsonElement;

        public List(Minecraft mcIn) {
            super(mcIn, 0, GuiCredits.this.height, 24, GuiCredits.this.height - 61, 18);

            int newWidth = GuiCredits.this.width / 6;

            for (JsonElement element: jsonArray) {
                JsonObject jsonObject = (JsonObject) element;
                String name = jsonObject.get("Name").getAsString();
                newWidth = (int)MathHelper.absMax(newWidth,renderer.getStringWidth(name) + 10);
                creditList.add(name);
            }
            setDimensions(newWidth,this.height,this.top,this.bottom);

        }

        protected int getSize()
        {
            return this.creditList.size();
        }

        @Override
        protected void elementClicked(int slotIndex, boolean isDoubleClick, int mouseX, int mouseY) {
            GuiCredits.this.selectedCredit = slotIndex;
            GuiCredits.this.update();
        }

        protected void overlayBackground(int startY, int endY, int startAlpha, int endAlpha) {}

        /**
         * Returns true if the element passed in is currently selected
         */
        protected boolean isSelected(int slotIndex)
        {
            return slotIndex == GuiCredits.this.selectedCredit;
        }

        /**
         * Return the height of the content being scrolled
         */
        protected int getContentHeight() {
            return this.getSize() * 18;
        }

        public void drawBackground() {}

        protected void drawSlot(int slotIndex, int xPos, int yPos, int heightIn, int mouseXIn, int mouseYIn, float partialTicks)
        {
            GuiCredits.this.drawCenteredString(GuiCredits.this.fontRenderer, this.creditList.get(slotIndex), this.width / 2, yPos + 1, 16777215);
        }
    }

    private class Info extends GuiScrollingList {
        private java.util.List<ITextComponent> lines = null;

        public Info(int width, java.util.List<String> lines)
        {
            super(GuiCredits.this.mc,
                    width,
                    GuiCredits.this.height,
                    24, GuiCredits.this.height - 24,
                    GuiCredits.this.list.width + 10, 60,
                    GuiCredits.this.width,
                    GuiCredits.this.height);
            this.lines    = resizeContent(lines);
            this.setHeaderInfo(true, getHeaderHeight());
        }

        @Override protected int getSize() { return 0; }
        @Override protected void elementClicked(int index, boolean doubleClick) { }
        @Override protected boolean isSelected(int index) { return false; }
        @Override protected void drawBackground() {}
        @Override protected void drawSlot(int slotIdx, int entryRight, int slotTop, int slotBuffer, Tessellator tess) { }

        private java.util.List<ITextComponent> resizeContent(java.util.List<String> lines) {
            LegitBase.CLIENT_INSTANCE.LOGGER.info("Content Lines (In): " + lines.size());
            java.util.List<ITextComponent> ret = new ArrayList<ITextComponent>();
            for (String line : lines)
            {
                if (line == null)
                {
                    ret.add(null);
                    continue;
                }

                ITextComponent chat = ForgeHooks.newChatWithLinks(line, false);
                int maxTextLength = (GuiCredits.this.width - 24) - (GuiCredits.this.list.width + 24) - 8;
                if (maxTextLength >= 0)
                {
                    ret.addAll(GuiUtilRenderComponents.splitText(chat, maxTextLength, GuiCredits.this.fontRenderer, false, true));
                }
            }
            LegitBase.CLIENT_INSTANCE.LOGGER.info("Content Lines (Out): " + ret.size());
            return ret;
        }

        private int getHeaderHeight()
        {
            int height = 0;
            height += (lines.size() * 10);
            if (height < this.bottom - this.top - 8) height = this.bottom - this.top - 8;
            return height;
        }


        @Override
        protected void drawHeader(int entryRight, int relativeY, Tessellator tess)
        {
            int top = relativeY;

            for (ITextComponent line : lines)
            {
                if (line != null)
                {
                    GlStateManager.enableBlend();
                    GuiCredits.this.fontRenderer.drawStringWithShadow(line.getFormattedText(), this.left + 4, top, 0xFFFFFF);
                    GlStateManager.disableAlpha();
                    GlStateManager.disableBlend();
                }
                top += 10;
            }
        }

        @Override
        protected void clickHeader(int x, int y)
        {
            int offset = y;
            if (offset <= 0)
                return;

            int lineIdx = offset / 10;
            if (lineIdx >= lines.size())
                return;

            ITextComponent line = lines.get(lineIdx);
            if (line != null)
            {
                int k = -4;
                for (ITextComponent part : line) {
                    if (!(part instanceof TextComponentString))
                        continue;
                    k += GuiCredits.this.fontRenderer.getStringWidth(((TextComponentString)part).getText());
                    if (k >= x)
                    {
                        GuiCredits.this.handleComponentClick(part);
                        break;
                    }
                }
            }
        }
    }
}
