package com.herobone.Legit.module.modules.misc;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.event.events.Render2DEvent;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.utils.RenderUtils;
import com.herobone.Legit.valuesystem.Value;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;


@ModuleInfo(moduleName = "PlayerFinder", moduleDescription = "", moduleCateogry = ModuleCategory.MISC)
public class PlayerFinder extends Module {

    public int distance;
    public double x;
    public double y;
    public double z;

    public Value<String> playerName = new Value<>("Name", null);

    public EntityPlayer player;

    @EventTarget
    public void onUpdate(EventUpdate event) {

        if (playerName.getObject() != null) {
            if (player == null) {
                player = mc.world.getPlayerEntityByName(playerName.getObject());
                return;
            }

            x = player.posX;
            y = player.posY;
            z = player.posZ;

            distance = (int) player.getDistance(mc.player);
        }

    }

    @EventTarget
    public void onRender2D(final Render2DEvent event) {
        if(!getState())
            return;

        final FontRenderer fontRenderer = mc.fontRenderer;
        final ScaledResolution scaledResolution = new ScaledResolution(mc);

        if (event.getState() == Render2DEvent.State.PRE) {
            fontRenderer.drawString("§aTarget Info   Distance: §f" + distance + "m", 25, scaledResolution.getScaledHeight() - 21, 0xFFFFFF);
            fontRenderer.drawString("§aX: §f" + (int) x, 25, scaledResolution.getScaledHeight() - 11, 0xFFFFFF);
            fontRenderer.drawString("§aY: §f" + (int) y, 62, scaledResolution.getScaledHeight() - 11, 0xFFFFFF);
            fontRenderer.drawString("§aZ: §f" + (int) z, 97, scaledResolution.getScaledHeight() - 11, 0xFFFFFF);

            GlStateManager.disableBlend();
        }

    }

}
