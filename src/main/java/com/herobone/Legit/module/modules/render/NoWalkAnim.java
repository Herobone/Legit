package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;


@ModuleInfo(moduleName = "NoWalkAnim", moduleDescription = "No Walk Animation", moduleCateogry = ModuleCategory.RENDER)
public class NoWalkAnim extends Module {

    @EventTarget
    public void onUpdate(EventUpdate e) {
        if(!getState())
            return;

        mc.player.distanceWalkedModified = 0.0F;
    }

}
