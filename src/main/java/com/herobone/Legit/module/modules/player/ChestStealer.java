package com.herobone.Legit.module.modules.player;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.utils.Time;
import com.herobone.Legit.valuesystem.Value;
import de.Hero.settings.Setting;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.ContainerChest;
import org.lwjgl.input.Keyboard;


@ModuleInfo(moduleName = "ChestStealer", moduleDescription = "Steal Chests", moduleCateogry = ModuleCategory.PLAYER, defaultKey = Keyboard.KEY_C)
public class ChestStealer extends Module {

    public Time time = new Time();
    public Time timedelay = new Time();

    private Value<Double> delay = new Value<>("Delay", 0.150);
    private Value<Double> startDelay = new Value<>("StartDelay", 0.250);

    public ChestStealer() {
        LegitBase.setmgr.rSetting(new Setting("Delay", this, delay, 0, 1));
        LegitBase.setmgr.rSetting(new Setting("Start Delay", this, startDelay, 0, 1));
    }

    @Override
    public void onEnable() {
        time.reset();
        timedelay.reset();
    }

    @EventTarget
    public void onUpdate(EventUpdate e) {
        if (!getState() || mc.world == null)
            return;

        if (!(mc.player.openContainer instanceof ContainerChest))
            timedelay.reset();

        if (mc.player.openContainer != null && mc.player.openContainer instanceof ContainerChest) {
            ContainerChest chest = (ContainerChest) mc.player.openContainer;
            if (timedelay.over((int) (startDelay.getObject()* 1000))) {
                for (int i = 0; i < chest.getLowerChestInventory().getSizeInventory(); i++) {
                    if (time.over((int) (delay.getObject() * 1000)) && chest.getLowerChestInventory().getStackInSlot(i).getCount() > 0) {
                        mc.playerController.windowClick(chest.windowId, i, 0, ClickType.QUICK_MOVE, mc.player);
                        time.reset();
                    }
                }
            }
        }
    }

}
