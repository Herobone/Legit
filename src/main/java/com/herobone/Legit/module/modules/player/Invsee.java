package com.herobone.Legit.module.modules.player;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.command.commands.InvseeCommand;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.utils.Time;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiInventory;

@ModuleInfo(moduleName = "Invsee", moduleDescription = "Use with .invsee <PlayerName>", moduleCateogry = ModuleCategory.PLAYER)
public class Invsee extends Module {
    
    public Time timedelay = new Time();
    private boolean activated = false;

    @Override
    public void onEnable() {
        LegitBase.CLIENT_INSTANCE.LOGGER.info("Open Invsee");
    }


    @EventTarget
    public void onUpdate(EventUpdate e) {
        if (!getState() || mc.world == null || activated) {
            activated = false;
            timedelay.reset();
            return;
        }

        GuiScreen gui = new GuiInventory(InvseeCommand.instance.player);
        gui.allowUserInput = false;
        mc.displayGuiScreen(gui);

        if (timedelay.over(100)) {
            activated = true;
            setState(false);
        }
    }
}