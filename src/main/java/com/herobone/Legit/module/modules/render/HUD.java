package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.Render2DEvent;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@ModuleInfo(moduleName = "HUD", moduleDescription = "The hud you know so well", moduleCateogry = ModuleCategory.RENDER)
public class HUD extends Module {

    public HUD() {
        setState(true);
    }

    @EventTarget
    public void onRender2D(final Render2DEvent event) {
        if(!getState())
            return;

        final FontRenderer fontRenderer = mc.fontRenderer;
        final ScaledResolution scaledResolution = new ScaledResolution(mc);

        if (event.getState() == Render2DEvent.State.PRE) {
            RenderUtils.drawRect(0, scaledResolution.getScaledHeight() - 23, scaledResolution.getScaledWidth(), scaledResolution.getScaledHeight(), Integer.MIN_VALUE);
            // Hotbar
            RenderUtils.drawRect(scaledResolution.getScaledWidth() / 2 - 91, scaledResolution.getScaledHeight() - 23, scaledResolution.getScaledWidth() / 2 + 91, scaledResolution.getScaledHeight(), Integer.MIN_VALUE);

            // Slot Overlay
            if(mc.player.inventory.currentItem == 0) {
                RenderUtils.drawRect(scaledResolution.getScaledWidth() / 2 - 91, scaledResolution.getScaledHeight() - 23, (scaledResolution.getScaledWidth() / 2 + 91)- 20 * 8, scaledResolution.getScaledHeight(), Integer.MAX_VALUE);
            } else {
                RenderUtils.drawRect((scaledResolution.getScaledWidth() / 2) - 91 + mc.player.inventory.currentItem * 20, scaledResolution.getScaledHeight() - 23 , (scaledResolution.getScaledWidth() / 2) + 91 - 20 * (8 - mc.player.inventory.currentItem), scaledResolution.getScaledHeight(), Integer.MAX_VALUE);
            }

            GlStateManager.disableBlend();
            fontRenderer.drawString("§bFPS: §f" + Integer.toString(Minecraft.getDebugFPS()), 2, 22, Integer.MAX_VALUE);

            DateFormat time = new SimpleDateFormat("HH:mm");
            Date now = Calendar.getInstance().getTime();
            String renderNowTime = time.format(now);
            fontRenderer.drawString(renderNowTime, scaledResolution.getScaledWidth() - 30, scaledResolution.getScaledHeight() - 11, RenderUtils.rainbowEffect(2000).getRGB());
        }


        GlStateManager.scale(2, 2, 2);
        fontRenderer.drawString(LegitBase.CLIENT_NAME, 2, 2, RenderUtils.rainbowEffect(764).getRGB(), true);
        GlStateManager.scale(0.5, 0.5, 0.5);

        final int[] yDist = {2};
        final int[] counter = {1};
        ModuleManager.getModules().stream().filter(Module :: getState).sorted(Comparator.comparingInt(module -> -fontRenderer.getStringWidth(module.getModuleName()))).forEach(module -> {
            if (module.getModuleCategory() != ModuleCategory.TARGETS) {
                fontRenderer.drawString(module.getModuleName(), scaledResolution.getScaledWidth() - 2 - fontRenderer.getStringWidth(module.getModuleName()), yDist[0], RenderUtils.rainbow(counter[0] * 300), true);
                yDist[0] += fontRenderer.FONT_HEIGHT;
                counter[0]++;
            }
        });

    }
}