package com.herobone.Legit.module.modules.movement;

import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;


@ModuleInfo(moduleName = "Sprint", moduleDescription = "Makes you sprint all the time", moduleCateogry = ModuleCategory.MOVEMENT)
public class Sprint extends Module {

    @EventTarget
    public void onpdate(EventUpdate e) {
        if(!getState())
            return;
        if(mc.player.moveForward > 0.0F) {
            if (!mc.player.isSneaking()) {
                if (mc.player.getFoodStats().getFoodLevel() > 6) {
                    mc.player.setSprinting(true);
                }
            }
        }
    }

}
