package com.herobone.Legit.module.modules.player;

import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.awt.*;
import java.awt.event.InputEvent;


@ModuleInfo(moduleName = "AutoSave", moduleDescription = "", moduleCateogry = ModuleCategory.PLAYER)
public class AutoSave extends Module {

    private double lastY = 0.0;
    private float maxFallHight = 18;
    private double fallDistance = 0f;

    @EventTarget
    public void onUpdate(EventUpdate e) {
        if (!getState())
            return;

        if (mc.player.onGround) {
            lastY = mc.player.posY;
        }

        fallDistance = lastY - mc.player.posY;

        if (fallDistance >= maxFallHight) {
            for (int i = 0; i < 9; i++) {
                ItemStack itemStack = mc.player.inventory.getStackInSlot(i);
                if (itemStack == null)
                    continue;
                Item item = itemStack.getItem();

                if (item == Items.BLAZE_ROD || item == Items.NETHER_STAR) {
                    mc.player.inventory.currentItem = i;
                    try {
                        Robot bot = new Robot();
                        bot.mousePress(InputEvent.BUTTON3_MASK);
                        bot.mouseRelease(InputEvent.BUTTON3_MASK);
                    } catch (AWTException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }

        maxFallHight = mc.player.getHealth() - 2;
    }

}
