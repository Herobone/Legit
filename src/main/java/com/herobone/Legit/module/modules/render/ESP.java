package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.valuesystem.Value;


@ModuleInfo(moduleName = "ESP", moduleDescription = "", moduleCateogry = ModuleCategory.RENDER)
public class ESP extends Module {
    public Value<Float> lineWidth = new Value<>("LineWidth", 3.0F);
}
