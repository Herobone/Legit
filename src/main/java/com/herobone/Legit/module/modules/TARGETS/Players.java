package com.herobone.Legit.module.modules.TARGETS;

import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;


@ModuleInfo(moduleName = "Players", moduleDescription = "", moduleCateogry = ModuleCategory.TARGETS)
public class Players extends Module {

    public boolean players = true;

    public Players() {
        setState(true);
    }

    @Override
    public void onDisable() {
        players = false;
    }

    @Override
    public void onEnable() {
        players = true;
    }
}
