package com.herobone.Legit.module.modules.world;

import com.herobone.Legit.Wrapper;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.MotionUpdateEvent;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import net.minecraft.block.BlockAir;
import net.minecraft.client.settings.KeyBinding;


@ModuleInfo(moduleName = "AutoEagle", moduleDescription = "", moduleCateogry = ModuleCategory.WORLD)
public class AutoEagle extends Module {

    @EventTarget
    public void onEagle(MotionUpdateEvent e) {
        if(!getState())
            return;

        if(e.getState() == MotionUpdateEvent.State.PRE) {
            if (Wrapper.getBlockUnderPlayer(mc.player) instanceof BlockAir) {
                if (mc.player.onGround) {
                    KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), true);
                }
            } else {
                if (mc.player.onGround) {
                    KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), false);
                }
            }
        }
    }

}
