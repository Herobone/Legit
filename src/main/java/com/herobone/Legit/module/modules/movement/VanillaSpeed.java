package com.herobone.Legit.module.modules.movement;

import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.utils.Timer;
import com.herobone.Legit.valuesystem.Value;


@ModuleInfo(moduleName = "VanillaSpeed", moduleDescription = "Vanilla Bunny-Hop", moduleCateogry = ModuleCategory.MOVEMENT)
public class VanillaSpeed extends Module {

    Timer delay = new Timer();
    Value<Integer> timerDelay = new Value<>("delay", 100);

    @Override
    public void onEnable() {
        delay.updateLastMS();
    }

    @EventTarget
    public void onUpdate(EventUpdate update) {
        if (!getState())
            return;

        if (mc.player.onGround && delay.hasTimePassed(timerDelay.getObject())) {
            mc.player.setJumping(true);
            delay.updateLastMS();
        }
    }

}
