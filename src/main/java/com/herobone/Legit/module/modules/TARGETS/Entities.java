package com.herobone.Legit.module.modules.TARGETS;

import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;


@ModuleInfo(moduleName = "Entities", moduleDescription = "", moduleCateogry = ModuleCategory.TARGETS)
public class Entities extends Module {

    public boolean entities = false;

    @Override
    public void onDisable() {
        entities = false;
    }

    @Override
    public void onEnable() {
        entities = true;
    }
}
