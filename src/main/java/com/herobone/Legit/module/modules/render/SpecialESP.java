package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.DoRenderEvent;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.module.modules.TARGETS.*;
import com.herobone.Legit.utils.RenderUtils;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.state.IBlockProperties;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityMinecartChest;
import net.minecraft.entity.item.EntityMinecartFurnace;
import net.minecraft.entity.item.EntityMinecartHopper;
import net.minecraft.tileentity.*;
import net.minecraft.util.math.BlockPos;
import org.lwjgl.input.Keyboard;


@ModuleInfo(moduleName = "SpecialESP", moduleDescription = "You can choose what you want to see", moduleCateogry = ModuleCategory.RENDER, defaultKey = Keyboard.KEY_P)
public class SpecialESP extends Module {

    @EventTarget
    public void onRender(DoRenderEvent event) {
        if (!getState())
            return;

        for (Object obj : Minecraft.getMinecraft().world.loadedTileEntityList) {
            if (obj instanceof TileEntityChest && ModuleManager.getModule(Chests.class).getState()) {
                BlockPos blockPos = ((TileEntityChest)obj).getPos();

                RenderUtils.boxESP(blockPos, 0, 255, 0, 255);
            }
            if (obj instanceof TileEntityEnderChest && ModuleManager.getModule(Enderchests.class).getState()) {
                BlockPos blockPos = ((TileEntityEnderChest)obj).getPos();

                RenderUtils.boxESP(blockPos, 0, 0, 255, 255);
            }
            if (obj instanceof TileEntityFurnace && ModuleManager.getModule(Furnaces.class).getState()) {
                BlockPos blockPos = ((TileEntityFurnace)obj).getPos();

                RenderUtils.boxESP(blockPos, 150, 150, 150, 255);
            }
            if (obj instanceof TileEntityHopper && ModuleManager.getModule(Hoppers.class).getState()) {
                BlockPos blockPos = ((TileEntityHopper)obj).getPos();

                RenderUtils.boxESP(blockPos, 255, 255, 255, 255);
            }

            if (obj instanceof TileEntityEnchantmentTable && ModuleManager.getModule(EnchantmentTable.class).getState()) {
                RenderUtils.specESP((TileEntityEnchantmentTable)obj, 50, 100, 255, 255);
            }

            if (obj instanceof TileEntityBed && ModuleManager.getModule(Beds.class).getState()) {
                RenderUtils.specESP((TileEntityBed)obj, 255, 255, 123, 255);
            }
        }

        if (ModuleManager.getModule(Minecarts.class).getState()) {
            for (Object obj : Minecraft.getMinecraft().world.loadedEntityList) {
                if (obj instanceof EntityMinecartChest) {
                    EntityMinecartChest entity = (EntityMinecartChest)obj;
                    BlockPos blockPos = new BlockPos(entity.posX, entity.posY, entity.posZ);

                    RenderUtils.boxESP(blockPos, 0, 255, 100, 255);
                }
                if (obj instanceof EntityMinecartFurnace && ModuleManager.getModule(Furnaces.class).getState()) {
                    EntityMinecartFurnace entity = (EntityMinecartFurnace)obj;
                    BlockPos blockPos = new BlockPos(entity.posX, entity.posY, entity.posZ);

                    RenderUtils.boxESP(blockPos, 151, 152, 153, 255);
                }
                if (obj instanceof EntityMinecartHopper && ModuleManager.getModule(Hoppers.class).getState()) {
                    EntityMinecartHopper entity = (EntityMinecartHopper)obj;
                    BlockPos blockPos = new BlockPos(entity.posX, entity.posY, entity.posZ);

                    RenderUtils.boxESP(blockPos, 191, 192, 193, 255);
                }
            }
        }
    }

}
