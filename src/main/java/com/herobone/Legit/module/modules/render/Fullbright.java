package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;


@ModuleInfo(moduleName = "Fullbright", moduleDescription = "", moduleCateogry = ModuleCategory.RENDER)
public class Fullbright extends Module {

    private float gammaSetting = 0F;

    @Override
    public void onEnable() {
        gammaSetting = mc.gameSettings.gammaSetting;
    }

    @Override
    public void onDisable() {
        mc.gameSettings.gammaSetting = gammaSetting;
    }

    @EventTarget
    public void onUpdate(EventUpdate e) {
        if (!getState())
            return;
        mc.gameSettings.gammaSetting = 100F;
    }
}
