package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.DoRenderEvent;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.tracers.IProjectilePhysics;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.Vec3d;

import java.util.Collection;


@ModuleInfo(moduleName = "Tracers", moduleDescription = "", moduleCateogry = ModuleCategory.RENDER)
public class Tracers extends Module {

    @EventTarget
    public void drawTrajectory(DoRenderEvent event)
    {
        if (!ModuleManager.getModule(Tracers.class).getState())
            return;
        Minecraft mc = Minecraft.getMinecraft();
        if (mc.gameSettings.hideGUI)
            return;
        Entity e = mc.getRenderViewEntity();
        if (e == null)
            e = mc.player;
        EntityPlayer player;
        if ((e instanceof EntityPlayer))
        {
            player = (EntityPlayer)e;
            for (IProjectilePhysics physics : LegitBase.CLIENT_INSTANCE.projectilePhysics) {
                if (player.getHeldItemMainhand() == null)
                    continue;
                if (physics.matchesItem(player.getHeldItemMainhand()))
                {
                    Collection<Vec3d> trajectory = physics.trajectory(player);


                    Tessellator tessellator = Tessellator.getInstance();
                    BufferBuilder worldRenderer = tessellator.getBuffer();

                    GlStateManager.pushMatrix();
                    GlStateManager.disableTexture2D();
                    GlStateManager.disableLighting();

                    GlStateManager.enableBlend();
                    GlStateManager.enableAlpha();

                    GlStateManager.translate(0.0F, player.eyeHeight , 0.0F);

                    worldRenderer.begin(3, DefaultVertexFormats.POSITION_COLOR);
                    for (Vec3d pos : trajectory) {
                        worldRenderer.pos(pos.x, pos.y, pos.z).color(0, 0, 255, 255).endVertex();
                    }

                    tessellator.draw();

                    GlStateManager.enableAlpha();
                    GlStateManager.disableBlend();

                    GlStateManager.enableLighting();
                    GlStateManager.enableTexture2D();
                    GlStateManager.popMatrix();
                }
            }
        }
    }

}
