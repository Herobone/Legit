package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.DoRenderEvent;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.module.ModuleManager;
import com.herobone.Legit.module.modules.TARGETS.*;
import com.herobone.Legit.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityMinecartChest;
import net.minecraft.entity.item.EntityMinecartFurnace;
import net.minecraft.entity.item.EntityMinecartHopper;
import net.minecraft.tileentity.*;
import net.minecraft.util.math.BlockPos;


@ModuleInfo(moduleName = "BlockESP", moduleDescription = "", moduleCateogry = ModuleCategory.RENDER)
public class BlockESP extends Module {

    public static BlockESP instance;

    public BlockESP() {
        instance = this;
    }

    @EventTarget
    public void onRender(DoRenderEvent event) {
        if (!getState())
            return;

        for (Object obj : Minecraft.getMinecraft().world.loadedTileEntityList) {
            if (obj instanceof TileEntityEndGateway && ModuleManager.getModule(EndPortal.class).getState()) {
                BlockPos blockPos = ((TileEntityEndGateway)obj).getPos();

                RenderUtils.boxESP(blockPos, 255, 0, 0, 255);
            }
        }
    }

}
