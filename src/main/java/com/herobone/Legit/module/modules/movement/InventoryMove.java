package com.herobone.Legit.module.modules.movement;

import com.herobone.Legit.Wrapper;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;


@ModuleInfo(moduleName = "InventoryMove", moduleDescription = "", moduleCateogry = ModuleCategory.MOVEMENT)
public class InventoryMove extends Module {

    @EventTarget
    public void onUpdate(EventUpdate e) {
        if (!getState())
            return;

        KeyBinding[] moveKeys = new KeyBinding[] {mc.gameSettings.keyBindForward, mc.gameSettings.keyBindBack, mc.gameSettings.keyBindLeft, mc.gameSettings.keyBindRight, mc.gameSettings.keyBindJump};

        KeyBinding[] arrayOfKeybindings = moveKeys;

        int i;
        int j;

        KeyBinding bind;

        if(!(mc.currentScreen instanceof GuiContainer) && !(mc.currentScreen instanceof GuiIngameMenu)) {
            if (mc.currentScreen == null) {
                j = moveKeys.length;

                for (i = 0; i < j; i++) {
                    bind = arrayOfKeybindings[i];
                    if (!Keyboard.isKeyDown(bind.getKeyCode())) {
                        KeyBinding.setKeyBindState(bind.getKeyCode(), false);
                    }
                }
            }
        } else {
            arrayOfKeybindings = moveKeys;
            j = moveKeys.length;

            for (i = 0; i < j; i++) {
                bind = arrayOfKeybindings[i];

                try {
                    Wrapper.getField(KeyBinding.class, "pressed").set(bind, Keyboard.isKeyDown(bind.getKeyCode()));
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                } catch (NoSuchFieldException e1) {
                    try {
                        Wrapper.getField(KeyBinding.class, "field_74513_e").set(bind, Keyboard.isKeyDown(bind.getKeyCode()));
                    } catch (IllegalAccessException e2) {
                        e2.printStackTrace();
                    } catch (NoSuchFieldException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

        if(mc.currentScreen instanceof  GuiContainer || mc.currentScreen instanceof GuiIngameMenu) {
            if(Keyboard.isKeyDown(200)) {
                --mc.player.rotationPitch;
            }
            if(Keyboard.isKeyDown(208)) {
                ++mc.player.rotationPitch;
            }
            if(Keyboard.isKeyDown(203)) {
                --mc.player.rotationYaw;
            }
            if(Keyboard.isKeyDown(205)) {
                ++mc.player.rotationYaw;
            }
        }
    }

}
