package com.herobone.Legit.module.modules.player;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.interfaces.IMixinMinecraft;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.utils.CallBack;
import com.herobone.Legit.utils.Time;
import com.herobone.Legit.valuesystem.Value;
import de.Hero.settings.Setting;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Mouse;

import java.awt.event.InputEvent;


@ModuleInfo(moduleName = "AutoClicker", moduleDescription = "", moduleCateogry = ModuleCategory.PLAYER)
public class AutoClicker extends Module {

    private Value<Integer> speed = new Value<Integer>("speed", 5, new CallBack(this, "updateTBC"));
    private long timeBetweenClicks = 1000 / speed.getObject();
    private Time time = new Time();

    public AutoClicker() {
        LegitBase.setmgr.rSetting(new Setting("Speed", this, speed, 5, 20));
    }

    public void updateTBC() {
        timeBetweenClicks = 1000 / speed.getObject();
        LegitBase.CLIENT_INSTANCE.LOGGER.info("Updtated the time beween clicks!");
    }

    @EventTarget
    public void onUpdate(EventUpdate eventUpdate) {
        if (!getState()) {
            time.reset();
            return;
        }

        if (time.over(timeBetweenClicks) && (Mouse.isButtonDown(InputEvent.BUTTON1_MASK) || Mouse.isButtonDown(InputEvent.BUTTON3_MASK))) {
            if (Mouse.isButtonDown(InputEvent.BUTTON1_MASK))
                ((IMixinMinecraft) Minecraft.getMinecraft()).click("right");
            if (Mouse.isButtonDown(InputEvent.BUTTON3_MASK))
                ((IMixinMinecraft) Minecraft.getMinecraft()).click("left");
            else
                throw new IllegalArgumentException("Button not registerd!");
            time.reset();
        }
    }
}
