package com.herobone.Legit.module.modules.player;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.Wrapper;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.EventUpdate;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

import java.awt.*;
import java.awt.event.InputEvent;


@ModuleInfo(moduleName = "AutoMLG", moduleDescription = "", moduleCateogry = ModuleCategory.PLAYER)
public class AutoMLG extends Module {

    private double lastY = 0.0;
    private float maxFallHight = 10;
    private double fallDistance = 0f;
    private boolean found = false;
    private Item choosenItem;
    private Item web = Item.getItemFromBlock(Blocks.WEB);
    private Item water = Items.WATER_BUCKET;

    @EventTarget
    public void onUpdate(EventUpdate e) {
        if (!getState())
            return;

        if (mc.player.onGround) {
            lastY = mc.player.posY;
            found = false;
        }

        fallDistance = lastY - mc.player.posY;

        if (fallDistance >= maxFallHight) {
            if (found) {
                place();
                return;
            }
            for (int i = 0; i < 9; i++) {
                ItemStack itemStack = mc.player.inventory.getStackInSlot(i);
                if (itemStack.getCount() == 0)
                    continue;
                Item item = itemStack.getItem();

                if (item == Item.getItemFromBlock(Blocks.WEB) || item == Items.WATER_BUCKET) {
                    mc.player.inventory.currentItem = i;
                    found = true;
                    choosenItem = item;
                }
            }
        }
    }

    private void place() {
        if (Wrapper.getBlockUnderPlayer(mc.player) == Blocks.WATER || Wrapper.getBlockUnderPlayer(mc.player) == Blocks.WEB || mc.player.inventory.getCurrentItem().getItem() == Items.BUCKET || Wrapper.getDistToGround(mc.player) > mc.playerController.getBlockReachDistance()) {
            LegitBase.CLIENT_INSTANCE.LOGGER.info("Water or web under Player");
            return;
        }
        /*try {
            Robot bot = new Robot();
            bot.mousePress(InputEvent.BUTTON3_MASK);
            bot.mouseRelease(InputEvent.BUTTON3_MASK);
        } catch (AWTException e1) {
            e1.printStackTrace();
        }*/
        if (choosenItem == water) {
            mc.playerController.processRightClick(mc.player, mc.world, EnumHand.MAIN_HAND);
        }
        if (choosenItem == web) {// Wrapper.getDistToGround(mc.player)
            mc.playerController.processRightClickBlock(mc.player, mc.world, new BlockPos(mc.player.posX, mc.player.posY - mc.playerController.getBlockReachDistance(), mc.player.posZ), EnumFacing.DOWN, new Vec3d(mc.player.posX, mc.player.posY,mc.player.posZ), EnumHand.MAIN_HAND);
        }
    }
}
