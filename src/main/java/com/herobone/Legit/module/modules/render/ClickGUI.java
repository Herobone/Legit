package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.LegitBase;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import de.Hero.settings.Setting;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;

@ModuleInfo(moduleName = "ClickGUI", moduleDescription = "This thingy here", moduleCateogry = ModuleCategory.RENDER, canEnable = false, defaultKey = Keyboard.KEY_RSHIFT)
public class ClickGUI extends Module {

    public ClickGUI() {
        ArrayList<String> options = new ArrayList<>();
        options.add("JellyLike");
        options.add("New");
        Setting setting = new Setting("Design", this, "New", options);
        LegitBase.setmgr.rSetting(setting);
        LegitBase.setmgr.rSetting(new Setting("Sound", this, false));
        LegitBase.setmgr.rSetting(new Setting("GuiRed", this, 255, 0, 255, true));
        LegitBase.setmgr.rSetting(new Setting("GuiGreen", this, 26, 0, 255, true));
        LegitBase.setmgr.rSetting(new Setting("GuiBlue", this, 42, 0, 255, true));
    }

    @Override
    public void onEnable() {
        mc.displayGuiScreen(LegitBase.CLIENT_INSTANCE.clickGUI);
        //mc.displayGuiScreen(new GuiInventory(mc.player));
        LegitBase.CLIENT_INSTANCE.LOGGER.info("Open ClickGui");
    }
}