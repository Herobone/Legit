package com.herobone.Legit.module.modules.render;

import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.DoRenderEvent;
import com.herobone.Legit.interfaces.IMixinMinecraft;
import com.herobone.Legit.module.Module;
import com.herobone.Legit.module.ModuleCategory;
import com.herobone.Legit.module.ModuleInfo;
import com.herobone.Legit.utils.CombatHelper;
import com.herobone.Legit.utils.GuiUtils;
import com.herobone.Legit.utils.RenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;


@ModuleInfo(moduleName = "NameTags", moduleDescription = "", moduleCateogry = ModuleCategory.RENDER)
public class NameTags extends Module implements RenderHelper, CombatHelper {
    public int color;
    private RenderItem itemRender;
    public static boolean armor = true;
    public static float scale = 1F;
    public static NameTags instance;

    public NameTags() {
        this.itemRender = new RenderItem(mc.renderEngine, ((IMixinMinecraft)mc).getModelManager(), new ItemColors());
        instance = this;
    }

    @EventTarget
    public void renderWorld(DoRenderEvent event) {
        if (getState()) {
            for (final Object i : mc.world.loadedEntityList) {
                if (i instanceof EntityPlayer) {
                    final EntityPlayer entity = (EntityPlayer)i;
                    if (entity.getName() == mc.player.getName()) {
                        continue;
                    }
                    final float posX = (float)((float)entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * ((IMixinMinecraft)mc).getTimer().renderPartialTicks);
                    final float posY = (float)((float)entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * ((IMixinMinecraft)mc).getTimer().renderPartialTicks);
                    final float posZ = (float)((float)entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * ((IMixinMinecraft)mc).getTimer().renderPartialTicks);
                    this.drawName(entity, posX - TileEntityRendererDispatcher.staticPlayerX, posY - TileEntityRendererDispatcher.staticPlayerY + entity.height + 0.550000011920929, posZ - TileEntityRendererDispatcher.staticPlayerZ);
                }
            }
        }
    }

    public void renderArmor(final Entity e) {
        final ArrayList<ItemStack> itemsToRender = new ArrayList<>();
        ItemStack heldStack = ((EntityPlayer)e).inventory.getCurrentItem();
        if (heldStack.getCount() > 0)
            itemsToRender.add(heldStack);

        for (ItemStack stack:((EntityPlayer)e).inventory.armorInventory) {
            if (stack.getCount() > 0) {
                itemsToRender.add(stack);
            }
        }
        int x = -(itemsToRender.size() * 8);
        final Iterator<ItemStack> iterator = itemsToRender.iterator();
        while (iterator.hasNext()) {
            final ItemStack stack = iterator.next();
            GlStateManager.pushMatrix();
            GlStateManager.depthMask(true);
            GlStateManager.clear(256);
            net.minecraft.client.renderer.RenderHelper.enableGUIStandardItemLighting();
            mc.getRenderItem().zLevel = -100.0f;
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();
            GlStateManager.disableBlend();
            GlStateManager.enableLighting();
            GlStateManager.enableDepth();
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();
            GlStateManager.disableAlpha();
            GlStateManager.disableAlpha();
            GlStateManager.disableBlend();
            GlStateManager.enableBlend();
            GlStateManager.enableAlpha();
            GlStateManager.enableAlpha();
            GlStateManager.enableLighting();
            GlStateManager.enableDepth();
            mc.getRenderItem().renderItemIntoGUI(stack, x, -18);
            mc.getRenderItem().renderItemOverlays(Minecraft.getMinecraft().fontRenderer, stack, x, -18);
            mc.getRenderItem().zLevel = 0.0f;
            GlStateManager.disableCull();
            GlStateManager.enableAlpha();
            GlStateManager.disableBlend();
            GlStateManager.disableLighting();
            GlStateManager.popMatrix();
            mc.getRenderItem().zLevel = 0.0f;
            net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
            final String text = "";
            if (stack != null) {
                int y = 0;
                final int sLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(16), stack);
                final int fLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(20), stack);
                final int kLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(19), stack);
                if (sLevel > 0) {
                    GL11.glDisable(2896);
                    drawEnchantTag("Sh" + sLevel, x, y);
                    y -= 9;
                }
                if (fLevel > 0) {
                    GL11.glDisable(2896);
                    drawEnchantTag("Fir" + fLevel, x, y);
                    y -= 9;
                }
                if (kLevel > 0) {
                    GL11.glDisable(2896);
                    drawEnchantTag("Kb" + kLevel, x, y);
                }
                else if (stack.getItem() instanceof ItemArmor) {
                    final int pLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(0), stack);
                    final int tLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(7), stack);
                    final int uLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(34), stack);
                    if (pLevel > 0) {
                        GL11.glDisable(2896);
                        drawEnchantTag("Pr" + pLevel, x, y);
                        y -= 9;
                    }
                    if (tLevel > 0) {
                        GL11.glDisable(2896);
                        drawEnchantTag("Th" + tLevel, x, y);
                        y -= 9;
                    }
                    if (uLevel > 0) {
                        GL11.glDisable(2896);
                        drawEnchantTag("Unb" + uLevel, x, y);
                    }
                }
                else if (stack.getItem() instanceof ItemBow) {
                    final int powLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(48), stack);
                    final int punLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(49), stack);
                    final int fireLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(50), stack);
                    if (powLevel > 0) {
                        GL11.glDisable(2896);
                        drawEnchantTag("Pow" + powLevel, x, y);
                        y -= 9;
                    }
                    if (punLevel > 0) {
                        GL11.glDisable(2896);
                        drawEnchantTag("Pun" + punLevel, x, y);
                        y -= 9;
                    }
                    if (fireLevel > 0) {
                        GL11.glDisable(2896);
                        drawEnchantTag("Fir" + fireLevel, x, y);
                    }
                }
                else if (stack.getRarity() == EnumRarity.EPIC) {
                    drawEnchantTag("§6God", x, y);
                }
                x += 16;
            }
        }
    }

    private void drawEnchantTag(final String text, int x, int y) {
        GlStateManager.pushMatrix();
        GlStateManager.disableDepth();
        x *= (int)1.75;
        y -= 4;
        GL11.glScalef(0.57f, 0.57f, 0.57f);
        mc.fontRenderer.drawStringWithShadow(text, x, -36 - y, -1);
        GlStateManager.enableDepth();
        GlStateManager.popMatrix();
    }

    public void drawName(final Entity e, final double posX, final double posY, final double posZ) {
        final EntityPlayer ep = (EntityPlayer)e;
        String name = ep.getDisplayName().getFormattedText();
        float scale = (float)(NameTags.scale / 20.0f + mc.player.getDistanceSq(e.posX, e.posY, e.posZ) / 10000.0);
        if (mc.player.getDistance(e.posX, e.posY, e.posZ) <= 256.0) {
            GlStateManager.pushMatrix();
            GlStateManager.translate(posX, posY, posZ);
            GlStateManager.glNormal3f(0.0f, 0.0f, 0.0f);
            GlStateManager.rotate(-mc.getRenderManager().playerViewY, 0.0f, 1.0f, 0.0f);
            GlStateManager.rotate(mc.getRenderManager().playerViewX, 1.0f, 0.0f, 0.0f);
            GlStateManager.scale(-scale, -scale, scale);
            if (scale >= 0.3f) {
                scale = 0.3f;
            }
            GlStateManager.scale(0.5, 0.5, 0.5);
            if (NameTags.armor) {
                this.renderArmor(e);
            }
            GlStateManager.scale(1.5, 1.5, 1.5);
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(770, 771);
            GlStateManager.scale(0.5, 0.5, 0.5);
            final int health = ((int) ep.getHealth());
            //final GuiUtils guiUtils = this.guiUtils;
            Gui.drawRect(-mc.fontRenderer.getStringWidth(String.valueOf(name) + " [" + health + "]") / 2 - 2, -2, mc.fontRenderer.getStringWidth(String.valueOf(name) + " [" + health + "]") / 2 + 2, 10, -1441722095);
            GlStateManager.enableTexture2D();
            GlStateManager.depthMask(true);
            mc.fontRenderer.drawString(String.valueOf(name) + " [" + health + "]", -mc.fontRenderer.getStringWidth(String.valueOf(name) + " [" + health + "]") / 2, 0.0f, -1, true);
            GlStateManager.scale(2.0, 2.0, 2.0);
            GlStateManager.enableLighting();
            GlStateManager.enableDepth();
            GlStateManager.disableBlend();
            GlStateManager.popMatrix();
        }
    }
}
