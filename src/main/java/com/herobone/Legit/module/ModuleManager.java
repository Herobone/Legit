package com.herobone.Legit.module;

import com.herobone.Legit.module.modules.TARGETS.*;
import com.herobone.Legit.module.modules.exploit.Respawn;
import com.herobone.Legit.module.modules.misc.PlayerFinder;
import com.herobone.Legit.module.modules.movement.InventoryMove;
import com.herobone.Legit.module.modules.movement.Sprint;
import com.herobone.Legit.module.modules.movement.VanillaSpeed;
import com.herobone.Legit.module.modules.player.*;
import com.herobone.Legit.module.modules.render.*;
import com.herobone.Legit.event.EventListener;
import com.herobone.Legit.event.EventTarget;
import com.herobone.Legit.event.events.KeyEvent;
import com.herobone.Legit.module.modules.world.AutoEagle;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
@SideOnly(Side.CLIENT)
public class ModuleManager implements EventListener {

    private static final List<Module> modules = new ArrayList<>();

    public void registerModules() {
        //MISC
        registerModule(new PlayerFinder());

        //WORLD
        registerModule(new AutoEagle());

        //EXPLOIT
        registerModule(new Respawn());

        //MOVEMENT
        registerModule(new Sprint());
        registerModule(new InventoryMove());
        registerModule(new VanillaSpeed());

        //RENDER
        registerModule(new ESP());
        registerModule(new BlockESP());
        registerModule(new SpecialESP());
        registerModule(new Tracers());
        registerModule(new NoHurtcam());
        registerModule(new NameTags());
        registerModule(new Fullbright());
        registerModule(new NoWalkAnim());
        registerModule(new HUD());
        registerModule(new ClickGUI());

        //PLAYER
        registerModule(new AutoClicker());
        registerModule(new ChestStealer());
        registerModule(new AutoSave());
        registerModule(new AutoMLG());
        registerModule(new Invsee());

        //TARGETS
        registerModule(new Players());
        registerModule(new Entities());
        registerModule(new Chests());
        registerModule(new Enderchests());
        registerModule(new Furnaces());
        registerModule(new Hoppers());
        registerModule(new EnchantmentTable());
        registerModule(new Minecarts());
        registerModule(new Beds());
        registerModule(new EndPortal());

    }

    public void registerModule(final Module module) {
        modules.add(module);
    }

    public static Module getModule(final Class<? extends Module> targetModule) {
        synchronized(modules) {
            for(final Module currentModule : modules)
                if(currentModule.getClass() == targetModule)
                    return currentModule;
        }

        return null;
    }

    public static Module getModule(final String targetModule) {
        synchronized(modules) {
            for(final Module currentModule : modules)
                if(currentModule.getModuleName().equalsIgnoreCase(targetModule))
                    return currentModule;
        }

        return null;
    }

    public static List<Module> getModules() {
        return modules;
    }

    @EventTarget
    public void onKey(KeyEvent event) {
        synchronized(modules) {
            modules.stream().filter(module -> module.getKeyBind() == event.getKey()).forEach(module -> module.setState(!module.getState()));
        }
    }
}
