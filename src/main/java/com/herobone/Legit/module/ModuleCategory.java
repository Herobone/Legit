package com.herobone.Legit.module;

/**
 * Project: LegitBase
 * -----------------------------------------------------------
 * Copyright © 2017 | CCBlueX | All rights reserved.
 */
public enum ModuleCategory {

    COMBAT("Combat"), PLAYER("Player"), MOVEMENT("Movement"), WORLD("World"), MISC("Misc"), EXPLOIT("Exploit"), RENDER("Render"), TARGETS("Targets");

    private String name;

    ModuleCategory(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}