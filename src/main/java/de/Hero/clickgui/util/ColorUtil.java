package de.Hero.clickgui.util;

import com.herobone.Legit.LegitBase;

import java.awt.Color;

/**
 *  Made by HeroCode
 *  it's free to use
 *  but you have to credit me
 *
 *  @author HeroCode
 */
public class ColorUtil {
	
	public static Color getClickGUIColor(){
		return new Color((int) LegitBase.setmgr.getSettingByName("GuiRed").getValDouble(), (int)LegitBase.setmgr.getSettingByName("GuiGreen").getValDouble(), (int)LegitBase.setmgr.getSettingByName("GuiBlue").getValDouble());
	}
}
