package de.Hero.settings;

import com.herobone.Legit.module.Module;
import com.herobone.Legit.valuesystem.Value;

import java.util.ArrayList;

/**
 *  Made by HeroCode
 *  it's free to use
 *  but you have to credit me
 *
 *  @author HeroCode
 */
public class Setting {
	
	private String name;
	private Module parent;
	private String mode;
	
	private String sval;
	private ArrayList<String> options;
	
	private boolean bval;
	
	private double dval;
	private double min;
	private double max;
	private boolean onlyint = false;

	public String type = "normal";
	private Value value;
	

	public Setting(String name, Module parent, String sval, ArrayList<String> options){
		this.name = name;
		this.parent = parent;
		this.sval = sval;
		this.options = options;
		this.mode = "Combo";
	}
	
	public Setting(String name, Module parent, boolean bval){
		this.name = name;
		this.parent = parent;
		this.bval = bval;
		this.mode = "Check";
	}
	
	public Setting(String name, Module parent, double dval, double min, double max, boolean onlyint){
		this.name = name;
		this.parent = parent;
		this.dval = dval;
		this.min = min;
		this.max = max;
		this.onlyint = onlyint;
		this.mode = "Slider";
	}

	public Setting(String name, Module parent, Value value, double min, double max) {
		this.name = name;
		this.parent = parent;
		this.type = "value";
		this.value = value;

		if (value.getObject() instanceof Boolean) {
			this.mode = "Check";
        } else if (value.getObject() instanceof Float || value.getObject() instanceof Double) {
            this.mode = "Slider";
            this.min = min;
            this.max = max;
        } else if (value.getObject() instanceof Integer) {
			this.mode = "Slider";
            this.min = min;
            this.max = max;
            this.onlyint = true;
		} else {
			throw new IllegalArgumentException();
		}
	}

    public Setting(String name, Module parent, Value value) {
        this.name = name;
        this.parent = parent;
        this.type = "value";
        this.value = value;

        if (value.getObject() instanceof Boolean) {
            this.mode = "Check";
        } else {
            throw new IllegalArgumentException();
        }
    }
	
	public String getName(){
		return name;
	}
	
	public Module getParentMod(){
		return parent;
	}
	
	public String getValString(){
		return this.sval;
	}
	
	public void setValString(String in){
		this.sval = in;
	}
	
	public ArrayList<String> getOptions(){
		return this.options;
	}
	
	public boolean getValBoolean(){
	    if (this.type.equals("normal"))
	        return this.bval;
	    else if (this.type.equals("value"))
	        return (boolean) this.value.getObject();
	    else
	        throw new IllegalArgumentException("not normal or even value");
	}
	
	public void setValBoolean(boolean in){
        if (this.type.equals("normal"))
            this.bval = in;
        else if (this.type.equals("value"))
            this.value.setObject(in);
        else
            throw new IllegalArgumentException("not normal or even value");
	}
	
	public double getValDouble(){
		if(this.onlyint){
			this.dval = (int)dval;
		}
        if (this.type.equals("normal"))
            return this.dval;
        else if (this.type.equals("value"))
            return Double.parseDouble(this.value.getObject().toString());
        else
            throw new IllegalArgumentException("not normal or even value");
	}

	public void setValDouble(double in){
        if (this.type.equals("normal"))
            this.dval = in;
        else if (this.type.equals("value")) {
            if (onlyint)
                this.value.setObject((int) in);
            else
                this.value.setObject(in);
        } else
            throw new IllegalArgumentException("not normal or even value");
	}
	
	public double getMin(){
		return this.min;
	}
	
	public double getMax(){
		return this.max;
	}
	
	public boolean isCombo(){
		return this.mode.equalsIgnoreCase("Combo");
	}
	
	public boolean isCheck(){
		return this.mode.equalsIgnoreCase("Check");
	}
	
	public boolean isSlider(){
		return this.mode.equalsIgnoreCase("Slider");
	}
	
	public boolean onlyInt(){
		return this.onlyint;
	}
}
